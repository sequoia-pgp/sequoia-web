import sys
from nltk import word_tokenize
from nltk.corpus import stopwords
stop = set(stopwords.words('english'))

def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

for line in sys.stdin:
    if not line.startswith(" "):
        continue
    line = "".join(filter(lambda c: c not in ",.:;", line))
    print(" ".join([i for i in line.split()
                    if i.lower() not in stop
                    and not is_int(i)
                    and len(i) > 1]))
