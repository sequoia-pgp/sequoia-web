How to build the web site
=========================

You need hugo to build the website.  On Debian, install it from the
'hugo' package:

    # apt install hugo

Furthermore, you need to checkout the submodules:

    sequoia-web $ git submodule init
    sequoia-web $ git submodule update

Now you can build the site using 'make', it will be put into 'public'.
To deploy the web site, use 'make deploy
TARGET=some.host.tld:target/path'.

Deployment
----------

To deploy the main web site, simply run `make deploy`.

To deploy a preview, override `HUGO_FLAGS` and `TARGET`.  For example,
to `preview.sequoia-pgp.org`, run:

    $ make deploy HUGO_FLAGS=--baseURL=https://preview.sequoia-pgp.org \
                  TARGET=sequoia@sequoia-pgp.org:preview.sequoia-pgp.org

Hacking
-------

We modified some parts of the theme.  To see the modifications, run:

    % find layouts -type f | while read F ; do if [ -e themes/*/$F ] ; then diff -u themes/*/$F $F ; fi ; done
