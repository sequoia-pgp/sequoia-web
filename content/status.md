+++
title = "Status"
keywords = ["status"]
+++

<style>th, td { padding: 0.1em 0.7em; /* XXX */ }</style>

This documents the implementation status of various standards in
Sequoia as of {{% lastmod %}}.  Sequoia has not been audited yet, due
to a lack of funding.

Please also see the [OpenPGP Interoperability Test
Suite](https://tests.sequoia-pgp.org/) for an automated assessment of
Sequoia's and other implementations' capabilities and how compatible
they are.

This document does **not** describe the status of high-level features
like smartcard support.  See the individual projects [on
Gitlab](https://gitlab.com/sequoia-pgp) or [on
GitHub](https://github.com/sequoia-pgp) for those details.

# OpenPGP

| RFC4880bis-06                   | RFC4880                  | Content                                                 | Status                | Notes                                                         |
|---------------------------------|--------------------------|---------------------------------------------------------|-----------------------|---------------------------------------------------------------|
| {{<rfc4880bis6 "2">}}           | {{<rfc4880 "2">}}        | General functions                                       | ✓                     |                                                               |
| {{<rfc4880bis6 "2.1">}}         | {{<rfc4880 "2.1">}}      | Confidentiality via Encryption                          | ✓                     |                                                               |
| {{<rfc4880bis6 "2.2">}}         | {{<rfc4880 "2.2">}}      | Authentication via Digital Signature                    | ✓                     |                                                               |
| {{<rfc4880bis6 "2.3">}}         | {{<rfc4880 "2.3">}}      | Compression                                             | ✓                     | See below for supported algorithms.                           |
| {{<rfc4880bis6 "2.4">}}         | {{<rfc4880 "2.4">}}      | Conversion to Radix-64                                  | ✓                     |                                                               |
| {{<rfc4880bis6 "2.5">}}         | {{<rfc4880 "2.5">}}      | Signature-Only Applications                             | ✓                     |                                                               |
| {{<rfc4880bis6 "3.2">}}         | {{<rfc4880 "3.2">}}      | Multiprecision Integers                                 | ✓                     |                                                               |
| {{<rfc4880bis6 "3.3">}}         | {{<rfc4880 "3.3">}}      | Key IDs                                                 | ✓                     |                                                               |
| {{<rfc4880bis6 "3.6">}}         | {{<rfc4880 "3.6">}}      | Keyrings                                                | {{<bug 189 done>}}    |                                                               |
| {{<rfc4880bis6 "3.7.1">}}       | {{<rfc4880 "3.7.1">}}    | String-to-Key (S2K) Specifier Types                     | ✓                     |                                                               |
| {{<rfc4880bis6 "3.7.2">}}       | {{<rfc4880 "3.7.2">}}    | String-to-Key Usage                                     | ✓                     |                                                               |
| {{<rfc4880bis6 "4.2.1">}}       | {{<rfc4880 "4.2.1">}}    | Old Format Packet Lengths                               | ✓                     |                                                               |
| {{<rfc4880bis6 "4.2.2">}}       | {{<rfc4880 "4.2.2">}}    | New Format Packet Lengths                               | ✓                     |                                                               |
| {{<rfc4880bis6 "4.3">}}         | {{<rfc4880 "4.3">}}      | Packet Tags                                             | ✓                     |                                                               |
| {{<rfc4880bis6 "5.1">}}         | {{<rfc4880 "5.1">}}      | Public-Key Encrypted Session Key Packets (Tag 1)        | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.1">}}       | {{<rfc4880 "5.2.1">}}    | Signature Types                                         | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.2">}}       | {{<rfc4880 "5.2.2">}}    | Version 3 Signature Packet Format                       | ✓                     | Since sequoia-openpgp 1.11.0.                                 |
| {{<rfc4880bis6 "5.2.3">}}       | {{<rfc4880 "5.2.3">}}    | Version 4 Signature Packet Format                       | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3">}}       |                          | Version 5 Signature Packet Format                       | ✗                     |                                                               |
| {{<rfc4880bis6 "5.2.3.1">}}     | {{<rfc4880 "5.2.3.1">}}  | Signature Subpacket Specification                       | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.4">}}     | {{<rfc4880 "5.2.3.4">}}  | Signature Creation Time                                 | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.5">}}     | {{<rfc4880 "5.2.3.5">}}  | Issuer                                                  | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.6">}}     | {{<rfc4880 "5.2.3.6">}}  | Key Expiration Time                                     | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.7">}}     | {{<rfc4880 "5.2.3.7">}}  | Preferred Symmetric Algorithms                          | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.8">}}     |                          | Preferred AEAD Algorithms                               | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.9">}}     | {{<rfc4880 "5.2.3.8">}}  | Preferred Hash Algorithms                               | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.10">}}    | {{<rfc4880 "5.2.3.9">}}  | Preferred Compression Algorithms                        | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.11">}}    | {{<rfc4880 "5.2.3.10">}} | Signature Expiration Time                               | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.12">}}    | {{<rfc4880 "5.2.3.11">}} | Exportable Certification                                | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.13">}}    | {{<rfc4880 "5.2.3.12">}} | Revocable                                               | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.14">}}    | {{<rfc4880 "5.2.3.13">}} | Trust Signature                                         | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.15">}}    | {{<rfc4880 "5.2.3.14">}} | Regular Expression                                      | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.16">}}    | {{<rfc4880 "5.2.3.15">}} | Revocation Key                                          | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.17">}}    | {{<rfc4880 "5.2.3.16">}} | Notation Data                                           | {{<bug 122 done>}}    |                                                               |
| {{<rfc4880bis6 "5.2.3.17.1">}}  |                          | The 'charset' Notation                                  |                       |                                                               |
| {{<rfc4880bis6 "5.2.3.17.2">}}  |                          | The 'manu' Notation                                     |                       |                                                               |
| {{<rfc4880bis6 "5.2.3.17.3">}}  |                          | The 'make' Notation                                     |                       |                                                               |
| {{<rfc4880bis6 "5.2.3.17.4">}}  |                          | The 'model' Notation                                    |                       |                                                               |
| {{<rfc4880bis6 "5.2.3.17.5">}}  |                          | The 'prodid' Notation                                   |                       |                                                               |
| {{<rfc4880bis6 "5.2.3.17.6">}}  |                          | The 'pvers' Notation                                    |                       |                                                               |
| {{<rfc4880bis6 "5.2.3.17.7">}}  |                          | The 'lot' Notation                                      |                       |                                                               |
| {{<rfc4880bis6 "5.2.3.17.8">}}  |                          | The 'qty' Notation                                      |                       |                                                               |
| {{<rfc4880bis6 "5.2.3.17.9">}}  |                          | The 'loc' and 'dest' Notations                          |                       |                                                               |
| {{<rfc4880bis6 "5.2.3.17.10">}} |                          | The 'hash' Notation                                     |                       |                                                               |
| {{<rfc4880bis6 "5.2.3.18">}}    | {{<rfc4880 "5.2.3.17">}} | Key Server Preferences                                  | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.19">}}    | {{<rfc4880 "5.2.3.18">}} | Preferred Key Server                                    | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.20">}}    | {{<rfc4880 "5.2.3.19">}} | Primary User ID                                         | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.21">}}    | {{<rfc4880 "5.2.3.20">}} | Policy URI                                              | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.22">}}    | {{<rfc4880 "5.2.3.21">}} | Key Flags                                               | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.23">}}    | {{<rfc4880 "5.2.3.22">}} | Signer's User ID                                        | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.24">}}    | {{<rfc4880 "5.2.3.23">}} | Reason for Revocation                                   | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.25">}}    | {{<rfc4880 "5.2.3.24">}} | Features                                                | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.26">}}    | {{<rfc4880 "5.2.3.25">}} | Signature Target                                        | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.27">}}    | {{<rfc4880 "5.2.3.26">}} | Embedded Signature                                      | ✓                     |                                                               |
| {{<rfc4880bis6 "5.2.3.28">}}    |                          | Issuer Fingerprint                                      | ✓                     |                                                               |
|                                 |                          | Intended Recipient                                      | ✓                     | Proposed.                                                     |
| {{<rfc4880bis6 "5.2.4">}}       | {{<rfc4880 "5.2.4">}}    | Computing Signatures                                    | ✓                     |                                                               |
| {{<rfc4880bis6 "5.3">}}         | {{<rfc4880 "5.3">}}      | Symmetric-Key Encrypted Session Key Packets (Tag 3) V4  | ✓                     |                                                               |
| {{<rfc4880bis6 "5.3">}}         |                          | Symmetric-Key Encrypted Session Key Packets (Tag 3) V5  | ✓                     |                                                               |
| {{<rfc4880bis6 "5.4">}}         | {{<rfc4880 "5.4">}}      | One-Pass Signature Packets (Tag 4)                      | ✓                     |                                                               |
| {{<rfc4880bis6 "5.5.2">}}       | {{<rfc4880 "5.5.2">}}    | Public-Key Packet Format V2                             | ✗                     | Obsolete.                                                     |
| {{<rfc4880bis6 "5.5.2">}}       | {{<rfc4880 "5.5.2">}}    | Public-Key Packet Format V3                             | ✗                     | Obsolete.                                                     |
| {{<rfc4880bis6 "5.5.2">}}       | {{<rfc4880 "5.5.2">}}    | Public-Key Packet Format V4                             | ✓                     |                                                               |
| {{<rfc4880bis6 "5.5.2">}}       |                          | Public-Key Packet Format V5                             | {{<bug 190>}}         |                                                               |
| {{<rfc4880bis6 "5.5.3">}}       | {{<rfc4880 "5.5.3">}}    | Secret-Key Packet Format V2                             | ✗                     | Obsolete.                                                     |
| {{<rfc4880bis6 "5.5.3">}}       | {{<rfc4880 "5.5.3">}}    | Secret-Key Packet Format V3                             | ✗                     | Obsolete.                                                     |
| {{<rfc4880bis6 "5.5.3">}}       | {{<rfc4880 "5.5.3">}}    | Secret-Key Packet Format V4                             | ✓                     |                                                               |
| {{<rfc4880bis6 "5.5.3">}}       |                          | Secret-Key Packet Format V5                             | {{<bug 123>}}         |                                                               |
| {{<rfc4880bis6 "5.6.1">}}       |                          | Algorithm-Specific Part for RSA Keys                    | ✓                     |                                                               |
| {{<rfc4880bis6 "5.6.2">}}       |                          | Algorithm-Specific Part for DSA Keys                    | ✓                     |                                                               |
| {{<rfc4880bis6 "5.6.3">}}       |                          | Algorithm-Specific Part for Elgamal Keys                | ✓                     |                                                               |
| {{<rfc4880bis6 "5.6.4">}}       |                          | Algorithm-Specific Part for ECDSA Keys                  | ✓                     |                                                               |
| {{<rfc4880bis6 "5.6.5">}}       |                          | Algorithm-Specific Part for EdDSA Keys                  | ✓                     |                                                               |
| {{<rfc4880bis6 "5.6.6">}}       |                          | Algorithm-Specific Part for ECDH Keys                   | ✓                     |                                                               |
| {{<rfc4880bis6 "5.7">}}         | {{<rfc4880 "5.6">}}      | Compressed Data Packet (Tag 8)                          | ✓                     |                                                               |
| {{<rfc4880bis6 "5.8">}}         | {{<rfc4880 "5.7">}}      | Symmetrically Encrypted Data Packet (Tag 9)             | ✗                     | Insecure.                                                     |
| {{<rfc4880bis6 "5.9">}}         | {{<rfc4880 "5.8">}}      | Marker Packet (Obsolete Literal Packet) (Tag 10)        | {{<bug 234 done>}}    |                                                               |
| {{<rfc4880bis6 "5.10">}}        | {{<rfc4880 "5.9">}}      | Literal Data Packet (Tag 11)                            | ✓                     |                                                               |
| {{<rfc4880bis6 "5.11">}}        | {{<rfc4880 "5.10">}}     | Trust Packet (Tag 12)                                   | {{<bug 235 done>}}    | Implementation defined.                                       |
| {{<rfc4880bis6 "5.12">}}        | {{<rfc4880 "5.11">}}     | User ID Packet (Tag 13)                                 | ✓                     |                                                               |
| {{<rfc4880bis6 "5.13">}}        | {{<rfc4880 "5.12">}}     | User Attribute Packet (Tag 17)                          | ✓                     |                                                               |
| {{<rfc4880bis6 "5.13.1">}}      | {{<rfc4880 "5.12.1">}}   | The Image Attribute Subpacket                           | {{<bug 191 done>}}    |                                                               |
| {{<rfc4880bis6 "5.13.2">}}      |                          | User ID Attribute Subpacket                             | ✗                     |                                                               |
| {{<rfc4880bis6 "5.14">}}        | {{<rfc4880 "5.13">}}     | Sym. Encrypted Integrity Protected Data Packet (Tag 18) | ✓                     |                                                               |
| {{<rfc4880bis6 "5.15">}}        | {{<rfc4880 "5.14">}}     | Modification Detection Code Packet (Tag 19)             | ✓                     |                                                               |
| {{<rfc4880bis6 "5.16">}}        |                          | AEAD Encrypted Data Packet (Tag 20)                     | ✓                     |                                                               |
| {{<rfc4880bis6 "5.16.1">}}      |                          | EAX Mode                                                | ✓                     |                                                               |
| {{<rfc4880bis6 "5.16.2">}}      |                          | OCB Mode                                                | ✗                     |                                                               |
| {{<rfc4880bis6 "6.2">}}         | {{<rfc4880 "6.2">}}      | Forming ASCII Armor                                     | ✓                     |                                                               |
| {{<rfc4880bis6 "7">}}           | {{<rfc4880 "7">}}        | Cleartext Signature Framework                           | {{<bug 151 done>}}    |                                                               |
| {{<rfc4880bis6 "8">}}           | {{<rfc4880 "8">}}        | Regular Expressions                                     | {{<bug 188 done>}}    |                                                               |
| {{<rfc4880bis6 "9.1">}}         | {{<rfc4880 "9.1">}}      | Public-Key Algorithms                                   | ✓                     | See below for supported algorithms.                           |
| {{<rfc4880bis6 "9.2">}}         |                          | ECC Curve OID                                           | ✓                     | See below for supported algorithms.                           |
| {{<rfc4880bis6 "9.3">}}         | {{<rfc4880 "9.2">}}      | Symmetric-Key Algorithms                                | ✓                     | See below for supported algorithms.                           |
| {{<rfc4880bis6 "9.4">}}         | {{<rfc4880 "9.3">}}      | Compression Algorithms                                  | ✓                     | See below for supported algorithms.                           |
| {{<rfc4880bis6 "9.5">}}         | {{<rfc4880 "9.4">}}      | Hash Algorithms                                         | ✓                     | See below for supported algorithms.                           |
| {{<rfc4880bis6 "9.6">}}         |                          | AEAD Algorithms                                         | ✓                     | See below for supported algorithms.                           |
| {{<rfc4880bis6 "11">}}          | {{<rfc4880 "11">}}       | Packet Composition                                      | ✓                     |                                                               |
| {{<rfc4880bis6 "11.1">}}        | {{<rfc4880 "11.1">}}     | Transferable Public Keys                                | ✓                     | We use a formal grammar.                                      |
| {{<rfc4880bis6 "11.2">}}        | {{<rfc4880 "11.2">}}     | Transferable Secret Keys                                | ✓                     | We use a formal grammar.                                      |
| {{<rfc4880bis6 "11.3">}}        | {{<rfc4880 "11.3">}}     | OpenPGP Messages                                        | ✓                     | We use a formal grammar.                                      |
| {{<rfc4880bis6 "11.4">}}        | {{<rfc4880 "11.4">}}     | Detached Signatures                                     | ✓                     |                                                               |
| {{<rfc4880bis6 "12.1">}}        | {{<rfc4880 "12.1">}}     | Key Structures V3                                       | ✗                     | Obsolete.                                                     |
| {{<rfc4880bis6 "12.1">}}        | {{<rfc4880 "12.1">}}     | Key Structures V4                                       | ✓                     |                                                               |
| {{<rfc4880bis6 "12.2">}}        | {{<rfc4880 "12.2">}}     | Key IDs and Fingerprints V3                             | ✗                     | Obsolete.                                                     |
| {{<rfc4880bis6 "12.2">}}        | {{<rfc4880 "12.2">}}     | Key IDs and Fingerprints V4                             | ✓                     |                                                               |
| {{<rfc4880bis6 "12.2">}}        |                          | Key IDs and Fingerprints V5                             | ✗                     |                                                               |
| {{<rfc4880bis6 "13">}}          |                          | Elliptic Curve Cryptography                             | ✓                     |                                                               |
| {{<rfc4880bis6 "13.1">}}        |                          | Supported ECC Curves                                    | ∂                     | See below for supported algorithms.                           |
| {{<rfc4880bis6 "13.2">}}        |                          | ECDSA and ECDH Conversion Primitives                    | ✓                     |                                                               |
| {{<rfc4880bis6 "13.3">}}        |                          | EdDSA Point Format                                      | ✓                     |                                                               |
| {{<rfc4880bis6 "13.4">}}        |                          | Key Derivation Function                                 | ✓                     |                                                               |
| {{<rfc4880bis6 "13.5">}}        |                          | EC DH Algorithm (ECDH)                                  | ✓                     |                                                               |

# Algorithms

We gracefully handle unknown algorithms during parsing and
serialization even if we do not support them.  This is important for
roundtripping OpenPGP packets.

What algorithms are supported by Sequoia depends on the cryptographic
backend selected at compile time.  Currently, the following backends
are available:

 - Nettle: Using the [Nettle cryptographic
   library](https://www.lysator.liu.se/~nisse/nettle/)
 - OpenSSL: Using the [OpenSSL cryptographic
   library](https://www.openssl.org/)
 - Botan: Using the [Botan cryptographic
   library](https://botan.randombit.net/)
 - CNG: Using Windows' [Cryptography API: Next
   Generation](https://docs.microsoft.com/en-us/windows/win32/seccng/cng-portal)
   (only available on Windows)
 - RustCrypto: Using cryptographic algorithms implemented in [pure
   Rust](https://github.com/RustCrypto)

## Public-Key Algorithms

| ID | Algorithm                                                       | Nettle | OpenSSL | Botan | CNG | RustCrypto | Notes                               |
|----|-----------------------------------------------------------------|--------|---------|-------|-----|------------|-------------------------------------|
| 1  | RSA (Encrypt or Sign)                                           | ✓      | ✓       | ✓     | ✓   | ✓          |                                     |
| 2  | RSA Encrypt-Only                                                | ✓      | ✓       | ✓     | ✓   | ✓          |                                     |
| 3  | RSA Sign-Only                                                   | ✓      | ✓       | ✓     | ✓   | ✓          |                                     |
| 16 | Elgamal (Encrypt-Only)                                          | ✗      | ✗       | ✓     | ✗   | ✗          |                                     |
| 17 | DSA (Digital Signature Algorithm)                               | ✓      | ✓       | ✓     | ✓   | ✗          |                                     |
| 18 | ECDH public key algorithm                                       | ✓      | ✓       | ✓     | ✓   | ✓          | See below for the supported curves. |
| 19 | ECDSA public key algorithm                                      | ✓      | ✓       | ✓     | ✓   | ✓          | See below for the supported curves. |
| 20 | Reserved (formerly Elgamal Encrypt or Sign)                     | ✗      | ✗       | ✗     | ✗   | ✗          | Insecure.                           |
| 21 | Reserved for Diffie-Hellman (X9.42, as defined for IETF-S/MIME) | ✗      | ✗       | ✗     | ✗   | ✗          |                                     |
| 22 | EdDSA                                                           | ✓      | ✓       | ✓     | ✓   | ✓          | See below for the supported curves. |
| 23 | Reserved for AEDH                                               | ✗      | ✗       | ✗     | ✗   | ✗          |                                     |
| 24 | Reserved for AEDSA                                              | ✗      | ✗       | ✗     | ✗   | ✗          |                                     |

### ECDH

| Curve name      | Nettle             | OpenSSL | Botan | CNG | RustCrypto | Notes                                   |
|-----------------|--------------------|---------|-------|-----|------------|-----------------------------------------|
| NIST P-256      | {{<bug 186 done>}} | ✓       | ✓     | ✓   | ✓          |                                         |
| NIST P-384      | {{<bug 186 done>}} | ✓       | ✓     | ✓   | ✗          |                                         |
| NIST P-521      | {{<bug 186 done>}} | ✓       | ✓     | ✓   | ✗          |                                         |
| brainpoolP256r1 | ✗                  | ✓       | ✓     | ✗   | ✗          |                                         |
| brainpoolP384r1 | ✗                  | ✓       | ✓     | ✗   | ✗          | Missing from `enum Curve` {{<bug 876>}} |
| brainpoolP512r1 | ✗                  | ✓       | ✓     | ✗   | ✗          |                                         |
| Curve25519      | ✓                  | ✓       | ✓     | ✓   | ✓          |                                         |

### ECDSA

| Curve name      | Nettle | OpenSSL | Botan | CNG | RustCrypto | Notes                                   |
|-----------------|--------|---------|-------|-----|------------|-----------------------------------------|
| NIST P-256      | ✓      | ✓       | ✓     | ✓   | ✓          |                                         |
| NIST P-384      | ✓      | ✓       | ✓     | ✓   | ✗          |                                         |
| NIST P-521      | ✓      | ✓       | ✓     | ✓   | ✗          |                                         |
| brainpoolP256r1 | ✗      | ✓       | ✓     | ✗   | ✗          |                                         |
| brainpoolP384r1 | ✗      | ✓       | ✓     | ✗   | ✗          | Missing from `enum Curve` {{<bug 876>}} |
| brainpoolP512r1 | ✗      | ✓       | ✓     | ✗   | ✗          |                                         |

### EdDSA

| Curve name | Nettle | OpenSSL | Botan | CNG | RustCrypto | Notes                                                             |
|------------|--------|---------|-------|-----|------------|-------------------------------------------------------------------|
| Ed25519    | ✓      | ✓       | ✓     | ✓   | ✓          | Implemented via [ed25519-dalek] when the CNG backend is selected. |

[ed25519-dalek]: https://crates.io/crates/ed25519-dalek

## Symmetric-Key Algorithms

| ID | Algorithm                         | Nettle             | OpenSSL | Botan | CNG | RustCrypto | Notes |
|----|-----------------------------------|--------------------|---------|-------|-----|------------|-------|
| 1  | IDEA                              | ✗                  | ✗       | ✓     | ✗   | ✓          |       |
| 2  | TripleDES (DES-EDE)               | ✓                  | ✓       | ✓     | ✓   | ✓          |       |
| 3  | CAST5 (128 bit key)               | {{<bug 193 done>}} | ✗       | ✓     | ✗   | ✓          |       |
| 4  | Blowfish (128 bit key, 16 rounds) | ✓                  | ✗       | ✓     | ✗   | ✓          |       |
| 7  | AES with 128-bit key              | ✓                  | ✓       | ✓     | ✓   | ✓          |       |
| 8  | AES with 192-bit key              | ✓                  | ✓       | ✓     | ✓   | ✓          |       |
| 9  | AES with 256-bit key              | ✓                  | ✓       | ✓     | ✓   | ✓          |       |
| 10 | Twofish with 256-bit key          | ✓                  | ✗       | ✓     | ✗   | ✓          |       |
| 11 | Camellia with 128-bit key         | ✓                  | ✓       | ✓     | ✗   | ✗          |       |
| 12 | Camellia with 192-bit key         | ✓                  | ✓       | ✓     | ✗   | ✗          |       |
| 13 | Camellia with 256-bit key         | ✓                  | ✓       | ✓     | ✗   | ✗          |       |

Note: OpenSSL's supported algorithms reflect the ones available in the
system's library. If system's OpenSSL supports all algorithms in this
table all of them will be exposed and available. Twofish is never
available since OpenSSL does not support it.

## Hash Algorithms

| ID | Algorithm | Nettle | OpenSSL | Botan | CNG | RustCrypto | Notes                             |
|----|-----------|--------|---------|-------|-----|------------|-----------------------------------|
| 1  | MD5       | ✓      | ✓       | ✓     | ✓   | ✓          | See below.                        |
| 2  | SHA1      | ✓      | ✓       | ✓     | ✓   | ✓          | Replaced by [SHA1CD].  See below. |
| 3  | RIPEMD160 | ✓      | ✓       | ✓     | ✗   | ✓          | See below.                        |
| 8  | SHA2-256  | ✓      | ✓       | ✓     | ✓   | ✓          |                                   |
| 9  | SHA2-384  | ✓      | ✓       | ✓     | ✓   | ✓          |                                   |
| 10 | SHA2-512  | ✓      | ✓       | ✓     | ✓   | ✓          |                                   |
| 11 | SHA2-224  | ✓      | ✓       | ✓     | ✗   | ✓          |                                   |

Weak algorithms are disallowed by default for contemporary messages by
the [StandardPolicy].  Furthermore, Sequoia uses a modified version of
SHA1 that mitigates known (and likely unknown attacks) on SHA1 called
[SHA1CD].

[StandardPolicy]: https://docs.sequoia-pgp.org/sequoia_openpgp/policy/struct.StandardPolicy.html#method.reject_hash_property_at
[SHA1CD]: https://github.com/cr-marcstevens/sha1collisiondetection

## Compression Algorithms

Support for compression algorithms is independent of the selected
cryptographic backend.

| ID | Algorithm    | Status | Notes |
|----|--------------|--------|-------|
| 0  | Uncompressed | ✓      |       |
| 1  | ZIP          | ✓      |       |
| 2  | ZLIB         | ✓      |       |
| 3  | BZip2        | ✓      |       |

# Related Functionality

## Streaming Operation

Safe processing of OpenPGP data requires streaming operation, which we
support on all levels.

## Public Key Store

Basic prototype exists.  Supports refreshing keys in the background.

## Key Server

| Aspect        | Status | Notes |
|---------------|--------|-------|
| [HKP(S)] get  | ✓      |       |
| [HKP(S)] send | ✓      |       |

[HKP(S)]: https://tools.ietf.org/html/draft-shaw-openpgp-hkp-00

## Web Key Directory

| Aspect              | Status | Notes |
|---------------------|--------|-------|
| Querying (direct)   | ✓      |       |
| Querying (advanced) | ✓      |       |
| Creating (direct)   | ✓      |       |
| Creating (advanced) | ✓      |       |

## Autocrypt

| Aspect                                                                     | Status | Notes                 |
|----------------------------------------------------------------------------|--------|-----------------------|
| [header parsing](https://autocrypt.org/level1.html#the-autocrypt-header)   | ✓      |                       |
| [keygen](https://autocrypt.org/level1.html#openpgp-based-key-data) V1      | ✓      |                       |
| [keygen](https://autocrypt.org/level1.html#openpgp-based-key-data) V1.1    | ✓      |                       |
| peer state                                                                 | ✗      |                       |
| header inject                                                              | ✗      |                       |
| recommend                                                                  | ✗      |                       |
| encrypt                                                                    | ✗      |                       |
| [setup message](https://autocrypt.org/level1.html#autocrypt-setup-message) | ✓      |                       |
| setup process                                                              | ✗      |                       |
| gossip                                                                     | ∂      | Parsing is supported. |
| uid decorative                                                             | ✓      |                       |
