---
title: "Sequoia PGP in Fedora Linux"
date: 2024-12-13T10:00:00+01:00
author: Fabio
banner: img/sequoia-banner.jpeg
---

Fedora 34 was the first version of Fedora to [ship Sequoia PGP][intro] back in
2021 - a lot has happened since then. In this post, I'll cover what's new, and
provide some hints for how to get started with some of the more advanced tools.

[intro]: https://bodhi.fedoraproject.org/updates/FEDORA-2021-c1fecf829f

<!--more-->

## Sequoia PGP takes over package management

In Fedora 38, the internal PGP implementation in RPM was replaced by
[rpm-sequoia][rpm-sequoia-change]. This implementation is also used by the dnf
package manager since version [4.17][dnf-4.17], replacing GPGME / GnuPG. These
changes have already trickled down into CentOS Stream 10 and
[RHEL 10 Beta][rhel-10b-rpm-sequoia]. Additionally, RHEL 10 Beta ships
[sq and sqv][rhel-10b-sq-sqv].

[rpm-sequoia-change]: https://fedoraproject.org/wiki/Changes/RpmSequoia
[dnf-4.17]: https://github.com/rpm-software-management/dnf/releases/tag/4.17.0
[rhel-10b-rpm-sequoia]: https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/10-beta/html-single/10.0_beta_release_notes/index#Jira-RHEL-47106
[rhel-10b-sq-sqv]: https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/10-beta/html-single/10.0_beta_release_notes/index#Jira-RHELPLAN-170379

But, of course, which packages are available is more interesting than the
package manager itself! The Fedora repositories include the latest versions of
almost all Sequoia PGP tools, including sq (`sequoia-sq`), sqv (`sequoia-sqv`),
the keyring linter (`sequoia-keyring-linter`), and the Sequoia PGP
implementation of the Stateless OpenPGP (SOP) interface (`sequoia-sop`) -- all
just one `dnf install` command away. These tools are also available from the
EPEL repositories for RHEL 9.

Additionally, drop-in replacements for Thunderbird's libRNP
(`sequoia-octopus-librnp`) and GnuPG command-line tools
(`sequoia-chameleon-gnupg`) are available too, but their setup is a *little*
more complicated than just installing them.

## Using the Octopus libRNP implementation in Thunderbird

The Thunderbird package in Fedora is set up to allow switching out the libRNP
implementation with the package manager. It defaults to the internal copy of
libRNP, but replacing it with [the Octopus][octopus-readme] (an implementation
of the libRNP library interface based on sequoia-openpgp) is easy:

[octopus-readme]: https://gitlab.com/sequoia-pgp/sequoia-octopus-librnp/-/blob/main/README.md

```
sudo dnf swap thunderbird-librnp sequoia-octopus-librnp
```

This drop-in replacement for libRNP has some advantages compared to stock
libRNP -- notably, it integrates with the user's GnuPG keyring and gpg-agent.

To revert to the default libRNP, just swap the package names in this command
to ... well, *swap* them back.

## Using the GnuPG Chameleon

The Chameleon provides drop-in replacements for the `gpg` and `gpgv` commands.
One of its main advantages is that it uses both the GnuPG and Sequoia
certificate stores, which removes the need to keep them in sync when switching
back and forth between GnuPG and Sequoia tools.

Installing the `gpg-sq` and `gpgv-sq` commands provided by the
[GnuPG Chameleon][chameleon-readme] is easy:

[chameleon-readme]: https://gitlab.com/sequoia-pgp/sequoia-chameleon-gnupg/-/blob/main/README.md

```
sudo dnf install sequoia-chameleon-gnupg
```

However, due to conflicts with the tools provided by the gnupg package (and lack
of a dpkg-divert-like mechanism in RPM), actually replacing the `gpg` command
with `gpg-sq` (and `gpgv` with `gpgv-sq`) is currently limited to user sessions,
and can't be done system-wide (yet).

The easiest method to use the executables provided by the Chameleon is to use
the "activation" script (similar to those used for Python virtualenvs) that is
included in the Fedora package:

```
source /usr/share/sequoia-chameleon-gnupg/activate
```

This sets up a shell environment (by modifying `$PATH`) where `gpg` and `gpgv`
point to the replacements from the sequoia-chameleon-gnupg package.

Making this change permanent for the whole user session is possible too by
creating a file at `~/.config/environment.d/sequoia-chameleon-gnupg.conf` with
the following contents:

```
PATH=/usr/share/sequoia-chameleon-gnupg/shims:$PATH
```

This is equivalent to using the "activation script", but is applied at session
initialization by systemd, which ensures that the `$PATH` change affects all
processes. To verify that this change has taken effect, check the output of the
`gpgconf` tool.

The `environment.d` mechanism only works for setting environment variables in
user sessions -- it cannot be used for system-wide overrides. But all user
applications which interface with the `gpg` or `gpgv` executables, or which use
wrapper libraries like GPGME, transparently use the executables from the
Chameleon.

