---
title: "Sequoia PGP: Out and About"
date: 2024-09-04T12:00:00+02:00
author: Neal
banner: img/sequoia-banner.jpeg
---

Over the past few months, we've attended a number of conferences.  In
addition to hearing from a lot of people who had helpful feedback and
fresh ideas, we've also held several presentations.

In this post, I summarize our talks, and link to recordings when they
are available.  I also report on the OpenPGP Email Summit, which is a
yearly gathering of some people from the OpenPGP community. (If you
are interested in the so-called *LibrePGP / OpenPGP schism*, read on.)

At the end, I list where you can meet us in person in the near future.
(Spoiler: at [Datenspuren](https://datenspuren.de/2024/) in Dresden in
September, and [IETF 121](https://www.ietf.org/meeting/121/) in Dublin
in November.)

<!--more-->

## *Chameleon - trying out Sequoia the easy way*

<script type="text/javascript">
<!--
  window.onload = function() {
    document.getElementById('video-player-holger')
      .addEventListener('click', function (e) {
        var span = document.getElementById('video-player-holger');
        span.innerHTML = '<video class="video-js vjs-default-skin vjs-big-play-centered" controls preload="auto" width="720" height="405" data-setup="{}"><source src="https://meetings-archive.debian.net/pub/debian-meetings/2024/DebConf24/debconf24-118-chameleon-the-easy-way-to-try-out-sequoia-openpgp-written-in-rust.av1.webm" type="video/webm"></video>';
      });
    document.getElementById('video-player-justus')
      .addEventListener('click', function (e) {
        var span = document.getElementById('video-player-justus');
        span.innerHTML = '<video class="video-js vjs-default-skin vjs-big-play-centered" controls preload="auto" width="720" height="405" data-setup="{}"><source src="https://meetings-archive.debian.net/pub/debian-meetings/2024/DebConf24/debconf24-135-sequoia-pgp-sq-gpg-from-sq-v6-openpgp-and-debian.av1.webm" type="video/webm"></video>';
      });
  };
 -->
</script>

<center>
  <b>Clicking on the thumbnail will load content from debian.net.</b>
  <br>
  <span id="video-player-holger">
    <img width="720" height="405" style="padding: 0em"
      src="holger-debconf-thumbnail.jpg"
      alt="Chameleon - the easy way to try out Sequoia - OpenPGP written in Rust"
      title="Clicking will load content from debian.net.">
  </span>
</center>
<br>

Holger started packaging Sequoia for Debian a year ago.  He's recently
created a presentation about getting started with Sequoia in Debian.
Over the past few months, he's presented it at [the miniDebConf
Berlin], [BornHack] in Denmark, and [DebConf 24] in South Korea.

  [the miniDebConf Berlin]: https://berlin2024.mini.debconf.org/talks/14-chameleon-trying-out-sequoia-the-easy-way-openpgp-written-in-rust/
  [BornHack]: https://bornhack.dk/bornhack-2024/program/chameleon-trying-out-sequoia-the-easy-way-openpgp-written-in-rust/
  [DebConf 24]: https://debconf24.debconf.org/talks/16-chameleon-the-easy-way-to-try-out-sequoia-openpgp-written-in-rust/

Holger's presentation can be summarized in one line:

```shell
$ sudo apt install gpg-from-sq
```

The above command installs the Chameleon in Debian testing.  The
Chameleon is our reimplementation of the `gpg` command-line interface.
We created the Chameleon to provide people an easy way to transition
from using GnuPG to Sequoia.  The main difficulty is that if a person
uses some programs that use GnuPG and simultaneously uses other
programs that use Sequoia, they will have two sets of keys,
certificates, and associated data that they have to constantly
reconcile.  This is annoying to say the least.  By providing a drop-in
replacement for `gpg`, users can switch to Sequoia all at once even if
the programs they use haven't been explicitly ported to Sequoia.  The
Chameleon was actually inspired by GnuPG, which started by
reimplementing PGP's CLI.

Immediately after Holger's presentations, there's been a surge of
interest in the Chameleon and Sequoia as measured by the number of bug
reports and questions on IRC.  Thanks a lot Holger!

## *Sequoia PGP, `sq`, `gpg-from-sq`, v6 OpenPGP, and Debian*

<center>
  <b>Clicking on the thumbnail will load content from debian.net.</b>
  <br>
  <span id="video-player-justus">
    <img width="720" height="405" style="padding: 0em"
      src="justus-debconf-thumbnail.jpg"
      alt="Sequoia PGP, sq, gpg-from-sq, v6 OpenPGP, and Debian"
      title="Clicking will load content from debian.net.">
  </span>
</center>
<br>

Two years ago, Justus presented an introduction to Sequoia for Debian
developers at [DebConf 22].  This year's presentation focused on what
has changed since then in both Sequoia itself, and more broadly in the
OpenPGP ecosystem.

  [DebConf 22]: https://debconf22.debconf.org/talks/71-sequoia-pgp-v5-openpgp-authentication-and-debian/

With respect to Sequoia, Justus described our architecture, our design
philosophy, and our various frontends.  The frontends include [`sq`],
our primary command line interface; [the Chameleon], our drop-in
replacement for gpg; [the Octopus], our replacement for Thunderbird's
OpenPGP implementation; and [`sqop`], our implementation of the
[Stateless OpenPGP interface] (SOP).

  [`sq`]: https://gitlab.com/sequoia-pgp/sequoia-sq#sq-the-sequoia-pgp-command-line-tool
  [the Chameleon]: https://gitlab.com/sequoia-pgp/sequoia-chameleon-gnupg
  [the Octopus]: https://gitlab.com/sequoia-pgp/sequoia-octopus-librnp
  [`sqop`]: https://gitlab.com/sequoia-pgp/sequoia-sop
  [Stateless OpenPGP interface]: https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/

Justus then spoke about the new OpenPGP RFC, [RFC 9580] (yes, we are
RFC!!! 😍).  He listed the highlights (authenticated encryption,
Argon2, non-deterministic signatures, padding, and SHA2-based
fingerprints), and he addressed the so-called schism.  The schism is
what the media is calling the OpenPGP and LibrePGP split.  LibrePGP is
Werner's fork of an earlier version of the OpenPGP draft.

  [RFC 9580]: https://datatracker.ietf.org/doc/rfc9580/

Justus explained his perspective on the situation.  After years of
being mostly dormant, [the working group rechartered] and [reviewed
the document proposed] by Werner.  The working group did not take all
of Werner's changes, and made many changes of their own.  These
changes included protection against [key overwriting attacks], a wire
format change to prevent [signature aliasing], and added protection
against [downgrade attacks].

  [the working group rechartered]: https://datatracker.ietf.org/doc/charter-ietf-openpgp/02/
  [reviewed the document proposed]: https://mailarchive.ietf.org/arch/msg/openpgp/CwEZ-Jd_NU2z59zkGrDEYOXVmKc/
  [key overwriting attacks]: https://www.kopenpgp.com/
  [signature aliasing]: https://datatracker.ietf.org/doc/minutes-115-openpgp-202211081300/
  [downgrade attacks]: https://eprint.iacr.org/2024/1110

Werner was unhappy with the changes proposed by the working group.  He
then disengaged, and published his own document, which he called
LibrePGP.  This document was no longer compatible with the OpenPGP
draft.  To reduce conflicts arising from parsing ambiguities if an
implementation were to support Werner's document and the OpenPGP
specification, [the working group decided to yield version 5 to
Werner], and switched the OpenPGP draft to version 6.  A few weeks
later, Werner published GnuPG 2.4.0, which started producing v5
artifacts.

  [the working group decided to yield version 5 to Werner]: https://mailarchive.ietf.org/arch/msg/openpgp/yayGaIen3DW6ixwrJkP-QcAcFSQ/

Justus observed that the same sequence of events is happening again.
The OpenPGP working group is currently working on standardizing
[post-quantum cryptography].  Werner has again decided to not engage
with the community.  Instead, he has forked the draft, changed it in
an incompatible way, and released GnuPG 2.5.0 with support for it.

The primary losers are the users.  Justus asks: What can we do?  Do we
want to live in a world where a broad community comes together to
produce a standard based on rough consensus, or accept a standard that
a king deigns to give us?  He prefers the former, and says that where
possible we should use standardized interfaces, and avoid vendor
lock-in.  And he backs that position up by suggesting that people not
directly use Sequoia where possible, but to instead prefer the
[Stateless OpenPGP interface] (SOP) when it is sufficient.

  [post-quantum cryptography]: https://datatracker.ietf.org/doc/draft-ietf-openpgp-pqc/

# OpenPGP Email Summit

Most of the Sequoia team attended this year's [OpenPGP Email Summit].
The OpenPGP Email Summit is a more or less yearly gathering of people
to discuss the technical details around encrypted emails using
OpenPGP.  Patrick Brunschwig, the author of Enigmail, founded the
summit in 2015, and has organized it since then.

The summit is designed primarily as a unconference.  That is, rather
than having prepared presentations, those present suggest topics,
which everyone then votes on.  The topics with the most interest are
then assigned to sessions.  The sessions aren't recorded, but [we take
detailed notes].

  [OpenPGP Email Summit]: https://www.openpgp.org/community/email-summit/2024/
  [we take detailed notes]: https://www.openpgp.org/community/email-summit/2024/minutes/

The [first session] this year was on a topic on everyone's mind: how
to deal with the schism arising from Werner's fork of the OpenPGP
standard.  Unfortunately, there was no way to work towards a
compromise: no one from GnuPG was present, and everyone who was
present expressed support for OpenPGP.

The first part of the discussion focused on how different people had
approached Werner to try and find a constructive way forward.  Despite
everyone's willingness to make concessions, everyone reported the same
result: Werner refused to budge.  Further, they all had the same
feeling: There is nothing that anyone can do to bring Werner back to
the community; Werner will not share power.

The second half of the discussion focused on what we can do given the
situation.  Everyone agreed that OpenPGP is socially, and technically
better, and has broad support within the community.  We cannot allow a
single person to dictate terms in order to avoid a conflict; we must
focus on collaboration, and what is best for users.  Bart Butler from
[Proton](https://proton.me/) captured the feeling in the room: "We are
moving on.  This is not war."  Despite the fear that the schism would
inject itself into every discussion, it wasn't raised in a substantial
way again.

  [first session]: https://www.openpgp.org/community/email-summit/2024/minutes/#session-1-schism-the-openpgp-schism

[Sylvester Tremmel](https://www.sylvester-tremmel.de/), a reporter
from Heise, was also present at the summit.  He and [Keywan
Tonekaboni](https://keywan.tonekaboni.de/) summarized their
perspective on the schism in [an article published on heise
online](https://www.heise.de/hintergrund/OpenPGP-im-Umbruch-Implementierungen-bessere-Standards-und-ein-grosser-Streit-9790850.html)
(German).

The rest of the sessions focused on various topics like post quantum
cryptography, header protection, forward compatibility, key
distribution, and how to migrated from v4 certificates to v6
certificates.

The organization of [the next summit] has already been started.  It
will again be near Frankfurt, Germany, and will take place from April
4 to April 6, 2025.  If you work on OpenPGP or have ideas about how
the ecosystem should evolve, do come.

  [the next summit]: https://www.openpgp.org/community/email-summit/2025/

## Other Appearances

I attended [Rust.nl 2024](https://2024.rustnl.org/),
[RustFest.ch](https://rustfest.ch/), [CoSin](https://cosin.ch/), and
[FrOSCon](https://froscon.org/).  These were great opportunities for
networking, and getting to know collaborators.  I also held a short
presentation at RustFest, *Sequoia PGP: A Not So New OpenPGP
Implementation in Rust*.  Unfortunately, the recording has not yet
been published.

## Upcoming Conferences

We'll be at [Datenspuren](https://datenspuren.de/2024/), which is
taking place in Dresden from September 20.-22.  I'll hold a talk (in
German) on Saturday at 14.30 entitled [*Sequoia PGP: Eine nicht mehr
ganz neue Implementierung von
OpenPGP*](https://talks.datenspuren.de/ds24/talk/JVWRHD/).

Justus will attend [IETF 121](https://www.ietf.org/meeting/121/) in
Dublin at the beginning of November where there is most certainly
going to be an OpenPGP Working Group session.

If you're attending either event, or are in the area, we'd be happy to
chat.
