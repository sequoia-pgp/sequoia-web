---
title: "Sequoia PGP: A Sapling Matures: Meet sq 1.0"
date: 2024-12-16T20:00:00+01:00
author: Neal
banner: img/sequoia-banner.jpeg
---

The Sequoia PGP team is happy to announce the release of version 1.0
of
[`sq`](https://gitlab.com/sequoia-pgp/sequoia-sq/-/blob/main/README.md).
`sq` is a command-line tool for working with OpenPGP artifacts with a
focus on usability, security, and robustness.

After [seven years of
development](https://gitlab.com/sequoia-pgp/sequoia/-/commits/b6fae6ab962f0061f18158d04c4ef6a7d06498cf),
this is `sq`'s first stable release.  A notable change for existing
users of `sq` is that we will no longer change `sq`'s CLI in an
incompatible manner.

<!--more-->

# A Short Introduction

`sq` is a command-line tool for working with OpenPGP artifacts.  In
addition to encryption, decryption, signing, and verification
services, `sq` provides a decentralized [public key
infrastructure](https://en.wikipedia.org/wiki/Public_key_infrastructure)
(PKI), and comprehensive key management facilities.

`sq` targets command-line users, package maintainers, and software
developers.  Although these users tend to handle complex systems well,
we've spent a lot of time ensuring that `sq` is easy to use, and
minimizes the cognitive burden on users.  To this end, we've rethought
existing concepts, and designed the tool around common workflows.

`sq` aims to be safe by default.  One way it achieves this is not by
having good defaults, but by avoiding defaults when it is not
absolutely clear that one option is and will remain much better than
the alternatives.  `sq` also avoids
[do-what-I-mean](https://en.wikipedia.org/wiki/DWIM) (DWIM)
interfaces.  At the beginning we feared that these design decisions
would decrease usability.  In practice, tab completion hides most of
the additional typing, and being explicit appears to reduce confusion.

`sq`'s PKI is probably its most notable feature, and the one we
invested the most time in.  The PKI is used to authenticate
certificates, and messages.  Authentication is necessary to ensure
that you are encrypting to the person you think you are, and to
identify who really authored a message; without authentication,
encryption and verification are much weaker.

`sq`'s PKI helps the user by opportunistically recording evidence.
When a certificate is downloaded from
[`keys.openpgp.org`](https://keys.openpgp.org), for instance, `sq`
automatically records that fact locally, and can display it when the
user later examines the certificate.  For users whose threat model
allows it, they can opt-in to using `keys.openpgp.org` as a trusted
introducer, and will rarely have to manually authenticate a
certificate.  This is safe in this threat model, because
`keys.openpgp.org` is a verifying key server.  For others, the
recorded information provides a helpful starting point to separate
fake certificates from authentic ones.  Fully trusting or partially
trusting a certification authority (CA), e.g., only relying on a CA
for a particular domain, is just as easy as using a global CA like
`keys.openpgp.org`.  This mechanism is a requirement for a federated
CA system, which we hope will emerge in the coming years.

# A Demo

Here's a small demonstration of how to download and verify a
[Qubes](https://www.qubes-os.org/) release.

First, we search for the certificate the Qubes project uses for
signing their releases.  We use `sq` to download the certificate from
the project's web page.  `sq` doesn't just fetch that file, it also
iterates, and looks for updates on various key servers, WKD and DANE:

```shell
$ sq network search https://keys.qubes-os.org/keys/qubes-release-4.2-signing-key.asc
Found 1 certificate related to the query:

 - 9C884DF3F81064A569A4A9FAE022E58F8E34D89F
   - Qubes OS Release 4.2 Signing Key (UNAUTHENTICATED)
   - created 2022-10-04 14:10:01 UTC
   - found via: hkps://keys.openpgp.org, hkps://keyserver.ubuntu.com, hkps://sks.pod01.fleetstreetops.com, http
Imported 1 new certificate, updated 0 certificates, 0 certificates unchanged, 0 errors.

Hint: After checking that the certificate 9C884DF3F81064A569A4A9FAE022E58F8E34D89F really belongs to the stated
      owner, you can mark the certificate as authenticated.  Each stated user ID can be marked individually using:

  $ sq pki link add --cert 9C884DF3F81064A569A4A9FAE022E58F8E34D89F "--userid=Qubes OS Release 4.2 Signing Key"
```

After verifying the fingerprint according to our threat model, we link
the certificate as suggested by the hint:

```shell
$ sq pki link add --cert 9C884DF3F81064A569A4A9FAE022E58F8E34D89F --userid "Qubes OS Release 4.2 Signing Key"
 - ┌ 9C884DF3F81064A569A4A9FAE022E58F8E34D89F
   └ Qubes OS Release 4.2 Signing Key
   - certification created
```

Finally, we simultaneously download and verify the Qubes image:

```shell
$ sq download --signature-url https://mirrors.edge.kernel.org/qubes/iso/Qubes-R4.2.3-x86_64.iso.asc --url https://mirrors.edge.kernel.org/qubes/iso/Qubes-R4.2.3-x86_64.iso --output /tmp/qubes.iso
 - ┌ 9C884DF3F81064A569A4A9FAE022E58F8E34D89F
   └ Qubes OS Release 4.2 Signing Key (authenticated)
 - 9C884DF3F81064A569A4A9FAE022E58F8E34D89F
   - created 2022-10-04 14:10:01 UTC

   - [    ✓    ] Qubes OS Release 4.2 Signing Key

     ◯─┬ 488780DC4EDF3D0FD536FF28C6025CF549CD0515
     │ └ (Local Trust Root)
     │
     │  certified the following binding on 2024‑12‑16
     │
     └─┬ 9C884DF3F81064A569A4A9FAE022E58F8E34D89F
       └ Qubes OS Release 4.2 Signing Key
Finished downloading data.  Authenticating data.
...
1 authenticated signature.
```

We only need to link the certificate once.  When downloading other
files published and signed by the Qubes project, `sq` will use the
existing link to authenticate the data.

# Learn More

If you want to try out `sq` and you have a Rust development
environment, you can use `cargo install` to get the latest version:

```shell
$ cargo install sequoia-sq --locked
```

Alternatively, you may already be able to get `sq` from your
distribution.  [Many
distributions](https://repology.org/project/sequoia-sq/versions) are
closely following Sequoia's development and provide updated packages
shortly after their release.

<script type="text/javascript">
<!--
  window.onload = function() {
    document.getElementById('video-player-neal')
      .addEventListener('click', function (e) {
        var span = document.getElementById('video-player-neal');
        span.innerHTML = '<iframe width="768" height="432" src="https://media.ccc.de/v/ds24-422-sequoia-pgp-a-not-quite-new-implementation-of-openpgp/oembed" frameborder="0" allowfullscreen></iframe>';
      });
  };
 -->
</script>

You can learn more about `sq`, by reading `sq`'s
[book](https://book.sequoia-pgp.org), and the [man
pages](https://sequoia-pgp.gitlab.io/sequoia-sq/man/sq.1.html). We've
also held several talks about Sequoia and `sq`.  The [most recent
presentation](https://media.ccc.de/v/ds24-422-sequoia-pgp-a-not-quite-new-implementation-of-openpgp)
was at [Datenspuren 2024](https://datenspuren.de/2024/):

<center>
  <b>Clicking on the thumbnail will load content from media.ccc.de.</b>
  <br>
  <span id="video-player-neal">
    <img width="768" height="432" style="padding: 0em"
      src="datenspuren.jpg"
      alt="Sequoia PGP: A not quite new implementation of OpenPGP"
      title="Clicking will load content from media.ccc.de.">
  </span>
</center>
<br>


# Thanks

Seven years is a long time, and there are a lot of people to thank!

The following people have contributed code to `sq`:

- Alexander Kjäll
- Andriy Utkin
- Azul
- Daiki Ueno
- Daniel Kahn Gillmor
- David Runge
- Devan Carpenter
- Fabio Valentini
- Franziska Schmidtke
- Gabriel de Perthuis
- Heiko Schaefer
- Holger Levsen
- Igor Matuszewski
- Jan Christian Grünhage
- Jens Reimann
- Justus Winter
- Lars Wirzenius
- Luca Saiu
- Malte Meiboom
- Neal H. Walfield
- Nora Widdecke
- Stacey Sheldon
- Wiktor Kwapisiewicz

The following people have opened issues regarding `sq`:

- [anarcat](https://gitlab.com/anarcat)
- [andrewgdotcom](https://gitlab.com/andrewgdotcom)
- [carlsmedstad](https://gitlab.com/carlsmedstad)
- [ctron](https://gitlab.com/ctron)
- [decathorpe](https://gitlab.com/decathorpe)
- [dkg](https://gitlab.com/dkg)
- [doronbehar](https://gitlab.com/doronbehar)
- [dvn0](https://gitlab.com/dvn0)
- [dvzrv](https://gitlab.com/dvzrv)
- [elioqoshi](https://gitlab.com/elioqoshi)
- [flow](https://gitlab.com/flow)
- [fschmidtke](https://gitlab.com/fschmidtke)
- [guillem](https://gitlab.com/guillem)
- [gunnarwolf](https://gitlab.com/gunnarwolf)
- [heftig](https://gitlab.com/heftig)
- [julian-klode](https://gitlab.com/julian-klode)
- [lepus2589](https://gitlab.com/lepus2589)
- [lucasaiu](https://gitlab.com/lucasaiu)
- [malte_meiboom](https://gitlab.com/malte_meiboom)
- [marmarek](https://gitlab.com/marmarek)
- [nwalfield](https://gitlab.com/nwalfield)
- [Occhioverde](https://gitlab.com/Occhioverde)
- [Prezu](https://gitlab.com/Prezu)
- [rickhg12hs](https://gitlab.com/rickhg12hs)
- [singpolyma](https://gitlab.com/singpolyma)
- [stac](https://gitlab.com/stac)
- [stratosgear](https://gitlab.com/stratosgear)
- [teythoon](https://gitlab.com/teythoon)
- [zamazan4ik](https://gitlab.com/zamazan4ik)

Finally, we'd like to thank the following individuals and institutions that have financed our work on Sequoia over the years:

- p≡p foundation
- [Sovereign Tech Agency](https://www.sovereign.tech/)
- [NLnet Foundation and the EU's NGI Assure Fund](https://nlnet.nl/)
- [Wau Holland Foundation](https://www.wauland.de/en/)
- [Phil Zimmermann](https://philzimmermann.com)
- [Proton](https://proton.me/)
