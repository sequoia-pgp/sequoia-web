---
title: "UX studies to test and improve sq"
date: 2024-06-18T12:15:00+02:00
author: Franzi
banner: img/sequoia-banner.jpeg
---

In a few months, we plan to release version 1.0 of `sq`, our primary command line interface.  With version 1.0, we will commit to a long-term stable API.  Ideally, that API will also be usable.  Although we've put in a lot of time thinking about usability, we want your feedback.  To this end, we're conducting a user study.  

<!--more-->

We are looking for volunteers who would like to try out our test parcour. You will be facing common operations related to public key cryptography (encryption, verification, web-of-trust). The study will be conducted remotely, and the session will be recorded and analysed later on.

The session will take approximately 45 to a maximum of 60 minutes, and we can offer a small thank you for your time. We will provide the tasks, take care of the recording and help interactively if problems arise.

We are specifically looking for people who are semi-regularly using OpenPGP, like package maintainers or security trainers. No background knowledge of Sequoia-PGP is required. 

If you are interested, please drop an email to ux-study@sequoia-pgp.org. If there are open questions, feel free to contact us [on irc at #sequoia on the OFTC network](https://webchat.oftc.net/?nick=&channels=%23sequoia). We don't have a specific schedule yet, but we will let you know in the next few weeks as the planning progresses further.

