---
title: "RFC9580 preview release"
date: 2024-12-23T14:00:00+01:00
author: Justus
banner: img/sequoia-banner.jpeg
---

The Sequoia PGP team is happy to announce the preview release of
version [2.0.0-alpha.0 of `sequoia-openpgp`].  [`sequoia-openpgp`] is
our low-level crate providing OpenPGP data types and associated
machinery

This is the first version that supports the new revision of OpenPGP
specified in [RFC9580] released at the end of July 2024.  It is the
successor of [RFC4880], released in 2007.  It brings new cryptographic
algorithms to OpenPGP, and deprecates and outright removes old ones.
Notably, it specifies AEAD, Argon2, and is the basis of the ongoing
[PQC work in OpenPGP].

[2.0.0-alpha.0 of `sequoia-openpgp`]: https://crates.io/crates/sequoia-openpgp/2.0.0-alpha.0
[`sequoia-openpgp`]: https://crates.io/crates/sequoia-openpgp
[RFC9580]: https://www.rfc-editor.org/rfc/rfc9580.html
[RFC4880]: https://www.rfc-editor.org/rfc/rfc4880
[PQC work in OpenPGP]: https://github.com/openpgp-pqc/draft-openpgp-pqc

<!--more-->

It has been four years since we released [version 1.0.0 of
`sequoia-openpgp`].  We are happy that the API held up quite nicely,
but there are a few warts and we made a few mistakes, and we knew that
in order to support [RFC9580] we would have to break the API.

[version 1.0.0 of `sequoia-openpgp`]: https://sequoia-pgp.org/blog/2020/12/16/202012-1.0/

Support for [RFC9580] is almost complete.  All of the functionality is
there, but there are a few rough spots, and some quality of life
improvements are missing.  Further, we want to keep the version 2.y.z
API stable at least as long as the 1.y.z API, and we want to use the
opportunity of having to break the API to fix some issues we
identified in the 1.y.z API.

Therefore, this is not the final 2.0.0 release, but a preview release.
We'd appreciate if you take a look, and maybe try to port your code to
it, but know that the API is not final, and will change before 2.0.0.

You can find a condensed list of changes in our [NEWS] file.  Or, you
can browse the [documentation].

[NEWS]: https://gitlab.com/sequoia-pgp/sequoia/-/blob/main/openpgp/NEWS?ref_type=heads#L7
[documentation]: https://docs.rs/sequoia-openpgp/2.0.0-alpha.0/sequoia_openpgp/
