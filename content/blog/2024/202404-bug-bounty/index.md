---
title: "Sequoia PGP gets a Bug Bounty Program"
date: 2024-04-10T11:00:00+02:00
author: Neal
banner: img/sequoia-banner.jpeg
---

The Sequoia PGP project now has a bug bounty program!  If you find a
novel security-relevant issue in almost any of our libraries,
applications, or specifications then you'll be rewarded with up to
€10,000.

<!--more-->

Last year, we were invited to join the [Sovereign Tech Fund]'s new
[Bug Resilience Program].  We accepted, and over the past few months,
we've been working on designing a bug bounty program.  The short
version is: if you find an issue in a Sequoia program, library, or
specification with a non-zero [CVSS] score, you're probably eligible
for a reward!

The bug bounty is hosted by [YesWeHack].  You can report bugs directly
via their platform, but that's not required.  Security issues that are
[responsibly disclosed] to us are also eligible.  You'll just need to
have an account with YesWeHack to claim the reward.

  [Sovereign Tech Fund]: https://www.sovereigntechfund.de/
  [Bug Resilience Program]: https://www.sovereigntechfund.de/programs/bug-resilience
  [CVSS]: https://en.wikipedia.org/wiki/Common_Vulnerability_Scoring_System
  [YesWeHack]: https://yeswehack.com
  [responsibly disclosed]: https://gitlab.com/sequoia-pgp/sequoia/-/blob/main/doc/security-vulnerabilities.md

The bug bounty's details follow.  (The [authoritative version] is
available on the YesWeHack platform.)

  [authoritative version]: https://yeswehack.com/programs/sequoia-pgp-bug-bounty-program

The bug bounty program officially opened last month in a closed beta.
Today, we're opening up the program to everyone.  We look forward to
hearing from you regarding both security vulnerabilities you've found,
and your [feedback] on the program.

  [feedback]: https://sequoia-pgp.org/contribute/#get-in-touch

## Bug Bounty Program

**Sequoia PGP** is a leading provider of secure communication and
  authentication in the OpenPGP space.

We are committed to protecting the privacy and security of our users
with a particular emphasis on the most vulnerable people in our society:
activists, journalists, lawyers and their clients.

We are working to achieve this goal by writing specifications, and
building libraries and programs for developers, adminstrators, and end
users. The specifications that we author, and the software that we
author is the sole scope of this bug bounty program.

This bug bounty program is paid for by the [Bug Resilience Program]
(https://www.sovereigntechfund.de/programs/bug-resilience).

### Program Rules

- We are convinced that external review by skilled security researchers
  is crucial to identifying weaknesses in our software.

- We are pleased to collaborate with you to resolve issues in our
  specifications and software, and to fairly reward you for the
  discovery of new security issues.

- Any type of attacks on our infrastructure, including our source code
  repositories is prohibited.

- When you report an issue, we must be able to reproduce it with our setup.

- An issue reported to us is considered to be a duplicate if it
  describes a similar attack to a known vulnerability (including issues
  received outside of YesWeHack) regardless of the component affected.
  That is, the triage team will use the "One Fix One Reward" process: if
  two or more programs or libraries use the same code base and a single
  fix can be deployed to fix all the others' weaknesses, only one issue
  will be considered as eligible for a reward, and other reports will be
  closed as Informative. We reward based on vulnerability, not per
  issue.

#### Important precautions and limitations

As a complement to the program rules and testing policy:

- **DO NOT include Personally Identifiable Information (PII)** in your
  report and please REDACT/OBFUSCATE the PII that is part of your PoC
  (screenshot, terminal transcripts, etc.) as much as possible.

- **DO NOT include secret key material** unless that has been created
    exclusively for testing purposes.

### Scopes

#### Underlying Libraries:

  - [buffered-reader](https://gitlab.com/sequoia-pgp/sequoia): A super-powered Reader
underlying our OpenPGP parser.
  - [nettle-sys](https://gitlab.com/sequoia-pgp/nettle-sys): Low-level Rust bindings for
the Nettle cryptographic library.
  - [nettle-rs](https://gitlab.com/sequoia-pgp/nettle-rs): Rust bindings for the Nettle
cryptographic library.
  - [SHA1-CD](https://gitlab.com/sequoia-pgp/sha1collisiondetection): A library and
command-line tool to detect SHA-1 collisions attacks in files.

#### OpenPGP-related Libraries:

  - [sequoia-openpgp](https://gitlab.com/sequoia-pgp/sequoia): A low-level OpenPGP library.
  - [sequoia-autocrypt](https://gitlab.com/sequoia-pgp/sequoia/-/tree/main/autocrypt?ref_type=heads): Autocrypt-related functionality.
  - [sequoia-ipc](https://gitlab.com/sequoia-pgp/sequoia/-/tree/main/ipc?ref_type=heads):
IPC-related functionality.
  - [sequoia-net](https://gitlab.com/sequoia-pgp/sequoia/-/tree/main/net?ref_type=heads):
Network-related functionality.
  - [Shared OpenPGP Certificate Directory](https://gitlab.com/sequoia-pgp/pgp-cert-d): A
low-level library to store OpenPGP certificates inspired by Maildir.
  - [sequoia-cert-store](https://gitlab.com/sequoia-pgp/sequoia-cert-store): A high-level
certificate store library.
  - [sequoia-wot](https://gitlab.com/sequoia-pgp/sequoia-wot): A web of trust engine.
  - [sequoia-policy-config](https://gitlab.com/sequoia-pgp/sequoia-policy-config): A
library for reading a cryptographic policy.
  - [rpm-sequoia](https://github.com/rpm-software-management/rpm-sequoia): The default
OpenPGP backend for RPM.

#### End user programs:

  - [sqv](https://gitlab.com/sequoia-pgp/sequoia-sqv): A simple signature verification
program.
  - [sq](https://gitlab.com/sequoia-pgp/sequoia-sq): Our primary command-line CLI.
  - [sqop](https://gitlab.com/sequoia-pgp/sequoia-sop): An implementation of the Stateless
OpenPGP (SOP) specification.
  - [octopus](https://gitlab.com/sequoia-pgp/sequoia-octopus-librnp): An alternative
OpenPGP backend for Thunderbird.
  - [sequoia-git](https://gitlab.com/sequoia-pgp/sequoia-git): A tool to check a commit
signing policy.

#### Specifications:

  - [OpenPGP Cert
Directory](https://gitlab.com/sequoia-pgp/pgp-cert-d/-/tree/main/spec?ref_type=heads): The
format and semantics of an on-disk certificate store.
  - [Web of
Trust](https://gitlab.com/sequoia-pgp/sequoia-wot/-/tree/main/spec?ref_type=heads): The
semantics of the web of trust.
  - [Sequoia
git](https://gitlab.com/sequoia-pgp/sequoia-git/-/tree/main/spec?ref_type=heads): How to
describe and check a signing policy for a git repository.

### Eligibility

We are happy to thank everyone who submits valid reports that help us
improve the security of Sequoia's specifications and software, however,
only those that meet the following eligibility requirements may receive
a monetary reward:

- You must be the first reporter of a vulnerability.

- The vulnerability must be a qualifying vulnerability (see below)

- You must send a clear textual description of the report along with
  steps to reproduce the issue, include attachments such as screenshots
  as necessary. PoC exploit code in the form of a unit test similar in
  style to those already present in the code (where applicable) is
  highly appreciated.

- You must not be a current employee of pEp Apps AG, or one of its contractors.

- Our analysis is always based on the worst impact demonstrated in your
  PoC

- When reviewing source-code, the "main" or "master" branches represent
  the current versions that are available as packages. Only reports for
  those branches will be eligible for bounty, if there is a master and
  main branch, the main branch takes precedence.

### Rating and Responsible Disclosure

We use CVSS to rate and categorize vulnerabilities. Any vulnerability
will be publicly disclosed after sufficient time has passed for
operating system distributions like Debian to deploy updates, and
dependant applications to prepare a new release.

Advisories will be published on our mailing-lists, and external
mailing-lists like oss-security. When appropriate we will also create an
ecosystem-specific advisory.  For instance, in the case of Rust code, we
will submit an advisory to [the rustsec advisory
database](https://rustsec.org/advisories/).

Please understand that we handle the full disclosure process and expect
that you do not disclose any findings yourself, we will include
researcher credits, if requested.

The process that we follow is described in [this
document](https://gitlab.com/sequoia-pgp/sequoia/-/blob/main/doc/security-vulnerabilities.md?ref_type=heads).

### Qualifying Vulnerabilities

- Memory safety issues (e.g. use-after-free, double free, aliasing,
  out-of-bounds access)

- Denial of service (e.g. due to crashes / panics, unbounded memory
  allocation, non-termination, algorithmic complexity attacks) with a
  full working PoC that demonstrate the real impact

- Undefined behavior leading to a security vulnerability (e.g. integer
  overflow)

- Injection attacks into human-readable and machine-readable output
  (like SigSpoof)

- Authentication bypasses (e.g. inadvertently encrypting to the wrong
  entity, saying a signature-protected message is authentic when it is
  not)

- Creation of cryptographic artifacts using high-level interfaces that
  break or undermine the properties of cryptographic algorithms
  (e.g. using a null key)

- Invalid reasoning about OpenPGP certificates (e.g. wrong binding
  signature used in reasoning)

- Exfiltration of confidential material (like the eFAIL attack)

- Diversion of packet streams with respect to another major OpenPGP
  implementation (i.e. presenting the same material to both and having
  the parsers disagree about what packets they see)

- Improper use of cryptographic primitives (e.g. nonce re-use)

- Improper handling of secret key material (e.g. use of secret key
  material leading to an exfiltration over a side-channel)

### Non-Qualifying Vulnerabilities

- Everything not in the qualifiying vulnerabilties list is not accepted

- Issues only found in outdated versions of our software (i.e. not
  vulnerable on the HEAD of the main or master branch)

- Issues found in dependencies, including our cryptographic backend
  libraries that does not impact directly our project

- Issues related to weak cryptographic algorithms that are rejected by
  [our standard policy](https://docs.rs/sequoia-openpgp/latest/sequoia_openpgp/policy/struct.StandardPolicy.html)
  in the default configuration on consumption

- Attacks that incorrectly use our API

### Rewards

 - Low: €500
 - Medium: €3,000
 - High: €5,000
 - Critical: €10,000
