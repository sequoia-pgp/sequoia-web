---
title: "Sequoia PGP, Community Outreach"
date: 2024-04-25T10:00:00+02:00
author: Neal
banner: img/sequoia-banner.jpeg
---

Since September 2023, nearly all paid work on Sequoia has been
[financed by the Sovereign Tech Fund] (STF).  The technical focus of
the award is on the maintenance and development of [sq], our
command-line front-end, and [sequoia-openpgp], our core library. But
the scope is not limited to development work: STF is also supporting
our standardization work, and community outreach.  In this blog post,
I'll highlight some our recent community work.

  [financed by the Sovereign Tech Fund]: https://www.sovereigntechfund.de/tech/sequoia-pgp-2023
  [sq]: https://gitlab.com/sequoia-pgp/sequoia-sq
  [sequoia-openpgp]: https://gitlab.com/sequoia-pgp/sequoia

<!--more-->

## Presentations

<script type="text/javascript">
<!--
  window.onload = function() {
    document.getElementById('video-player-moral')
      .addEventListener('click', function (e) {
        var span = document.getElementById('video-player-moral');
        span.innerHTML = '<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/9aHPhdflA2c?start=0&autoplay=1&cc_load_policy=1" title="YouTube video player" frameborder="0" allowfullscreen></iframe>';
      });
    document.getElementById('video-player-ietf')
      .addEventListener('click', function (e) {
        var span = document.getElementById('video-player-ietf');
        span.innerHTML = '<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/VpMpOXnbQig?start=533&autoplay=1&cc_load_policy=1" title="YouTube video player" frameborder="0" allowfullscreen></iframe>';
      });
    document.getElementById('video-player-fosdem')
      .addEventListener('click', function (e) {
        var span = document.getElementById('video-player-fosdem');
        span.innerHTML = '<video preload="none" controls="controls" width="75%"><source src="https://video.fosdem.org/2024/k1105/fosdem-2024-3297-sequoia-pgp-rethinking-openpgp-tooling.av1.webm" type=\'video/webm; codecs="av01.0.08M.08.0.110.01.01.01.0"\' /><source src="https://video.fosdem.org/2024/k1105/fosdem-2024-3297-sequoia-pgp-rethinking-openpgp-tooling.mp4" type="video/mp4" /></video>';
      });
  };
 -->
</script>

### *Sequoia PGP: Following a Moral Imperative* at Karakun AG

<center>
  <span id="video-player-moral">
    <b>Clicking on the thumbnail will load content from YouTube.</b>
    <br>
    <img width="560" height="315" style="padding: 0em"
      src="following-a-moral-imperative-thumbnail.jpg"
      alt="Sequoia PGP: Following a Moral Imperative presentation"
      title="Clicking will load content from YouTube.">
  </span>
</center>
<br>

In November, I delivered a talk entitled *Sequoia PGP: Following a
Moral Imperative* at [Karakun AG] in Basel, Switzerland.  I was
invited by Christian Ribeaud, who works at Karakun AG.  Karakun AG is
a software development company, and is one of the entities working
with the [Swiss Institute of Bioinformatics (SIB)] on a tool called
[sett], which hospitals and researchers in Switzerland use to exchange
privacy sensitive data.  (Here's a [presentation about sett].)  We've
been in regular contact with the sett developers since shortly after
[they decided to switch to Sequoia].

  [Karakun AG]: https://karakun.com/
  [Swiss Institute of Bioinformatics (SIB)]: https://www.sib.swiss/
  [sett]: https://gitlab.com/biomedit/sett
  [presentation about sett]: https://www.youtube.com/watch?v=cP7uy97uU4A
  [they decided to switch to Sequoia]: https://gitlab.com/biomedit/sett/-/issues/360

The first half of my presentation was a call to action.  Just because
a client asks you to implement some functionality doesn't mean that
you should do it.  As developers, we have the moral imperative to
reflect on the possible consequences of what we create, and, if
necessary, explain to our clients that violating human rights is
non-negotiable.  Further, we must resist requests to collect
unnecessary data, and we should add end-to-end encryption where
possible.

In the the second half of the talk, I presented background
information, and technical details about PGP, OpenPGP, and Sequoia
PGP.  I argued that Sequoia PGP is one possible option that developers
should consider to help protect users, but that the most important
thing they should do is to respect human rights; the actual technology
is secondary.

### Interop Testing v6 at IETF 118

<center>
  <span id="video-player-ietf">
    <b>Clicking on the thumbnail will load content from YouTube.</b>
    <br>
    <img width="560" height="315" style="padding: 0em"
      src="ietf-interop-testing-v6.jpg"
      alt="Interop Test v6"
      title="Clicking will load content from YouTube.">
  </span>
</center>
<br>

At IETF 118, Justus presented his work on our [Interoperability Test
Suite] in particular as regards the upcoming [OpenPGP revision].  The
so-called *crypto-refresh* has been submitted to the [IESG] for
publication.  The proposed standard includes AEAD encryption among
[many other modernizations].

  [Interoperability Test Suite]: https://tests.sequoia-pgp.org/
  [OpenPGP revision]: https://datatracker.ietf.org/doc/draft-ietf-openpgp-crypto-refresh/
  [IESG]: https://www.ietf.org/about/groups/iesg/
  [many other modernizations]: https://proton.me/blog/openpgp-crypto-refresh

Justus started the interoperability test suite four years ago.  As of
the presentation, it included 131 tests, 1510 test vectors, and tested
most major OpenPGP implementations.  Justus requested that anyone
working on an OpenPGP implementation implement the [SOP interface],
which the test suite uses to execute the tests, and tell him so that
he can add their implementation to the [test suite's report].

  [SOP interface]: https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/
  [test suite's report]: https://tests.sequoia-pgp.org/

### *Sequoia PGP: Rethinking OpenPGP Tooling presentation* at FOSDEM

<center>
  <span id="video-player-fosdem">
    <b>Clicking on the thumbnail will load content from fosdem.org.</b>
    <br>
    <img width="560" height="315" style="padding: 0em"
      src="rethinking-openpgp-tooling-thumbnail.jpg"
      alt="Sequoia PGP: Rethinking OpenPGP Tooling presentation"
      title="Clicking will load content from fosdem.org.">
  </span>
</center>
<br>

At FOSDEM, I presented [*Sequoia PGP: Rethinking OpenPGP Tooling*].
The talk was divided into two parts.

  [*Sequoia PGP: Rethinking OpenPGP Tooling*]: https://fosdem.org/2024/schedule/event/fosdem-2024-3297-sequoia-pgp-rethinking-openpgp-tooling/

In the first part, I told Sequoia's origin story.  I emphasized the
debt that Sequoia owes to Werner Koch, GnuPG's creator and our former
employer.  I explained that we decided to start Sequoia due to
differences in our technical visions; that although we had many
technical conversations with Werner, we were unable to find a common
ground.  Our intention with Sequoia was not to replace GnuPG or
somehow steal its users, but to provide users with another option
thereby growing OpenPGP's user base.  In that way, we hoped that not
only would the ecosystem grow, but privacy and security would improve
for all.

  [GnuPG]: https://www.gnupg.org

In the second half of the talk, I explained Sequoia's architecture,
and how it is not just a reimplementation of GnuPG, but has a
different philosophy, and embraces other paradigms.  In particular,
Sequoia takes a library-first approach, and strives for policy-free,
unopinionated, but safe-by-default interfaces.  Further, Sequoia
focuses on authentication for users with a variety of threat models:
from those who only care about their privacy, and are willing to rely
on central authorities like [CAs], to those who are worried about
their personal safety, and only want to rely on a few, highly select
entities.

  [CAs]: https://en.wikipedia.org/wiki/Certificate_authority

I concluded by reiterating that winning is not having Sequoia or
OpenPGP dominate the "market," but improving the privacy and security
of individuals.

## Conferences

Over the past few months, we attended [IETF 118 in Prague], [37C3],
[FOSDEM], and [MiniDebCamp Hamburg].  We had great talks with
developers from projects using Sequoia like [Qubes OS], [Proxmox] and
[Tumpa]; with developers from the broader OpenPGP ecosystem, like
those working on [DeltaChat], a decentralized chat app, which uses
mail for its transport layer, Proton's crypto team who maintain
[gopenpgp] and [openpgpjs], and [Thunderbird]'s e2ee team; and with
many other people.

  [IETF 118 in Prague]: https://datatracker.ietf.org/meeting/118/proceedings/overview/
  [37C3]: https://events.ccc.de/category/37c3/
  [FOSDEM]: https://fosdem.org/2024/
  [MiniDebCamp Hamburg]: https://wiki.debian.org/DebianEvents/de/2024/MiniDebCampHamburg
  [Qubes OS]: https://www.qubes-os.org/
  [Proxmox]: https://www.proxmox.com
  [Tumpa]: https://tumpa.rocks/
  [DeltaChat]: https://delta.chat/
  [gopenpgp]: https://github.com/ProtonMail/gopenpgp
  [openpgpjs]: https://github.com/openpgpjs/openpgpjs
  [Thunderbird]: https://www.thunderbird.net

In the coming months, we plan to be at [RustNL] (May, Netherlands),
[MiniDebConf Berlin] (May, Germany), the [OpenPGP Email Summit] (June,
Germany), and [DebConf24] (July, South Korea).  If you are attending
and want to chat, please feel free to [reach out].

  [OpenPGP Email Summit]: https://wiki.gnupg.org/OpenPGPEmailSummit202406
  [RustNL]: https://2024.rustnl.org/
  [MiniDebConf Berlin]: https://berlin2024.mini.debconf.org/
  [DebConf24]: https://debconf24.debconf.org/
  [reach out]: https://sequoia-pgp.org/contribute/

# Sequoia's Users

Sequoia doesn't exist in vacuum: Sequoia's APIs and functionality are
greatly influenced by our users.  The most salient feedback we get
comes when we engage with them.

### sett

The [Swiss Institute of Bioinformatics (SIB)] [commissioned] the
creation of [sett (Secure Encryption and Transfer Tool)] for use by
Swiss hospitals and researchers to exchange privacy sensitive data.

  [Swiss Institute of Bioinformatics (SIB)]: https://www.sib.swiss/
  [commissioned]: https://sphn.ch/wp-content/uploads/2020/06/sett_Infoblatt_v2.0.pdf
  [sett (Secure Encryption and Transfer Tool)]: https://gitlab.com/biomedit/sett

A few years ago, the developers of sett decided to switch to Sequoia
for their OpenPGP needs.  Since then, we've collaborated with them on
a regular basis.  We've [helped out], [contributed code], and [made a
few suggestions].  This past November, I also visited them in Basel.
In addition to the aforementioned presentation at Karakun, we had a
half day meeting during which we discussed some security-relevant
details of sett.

  [helped out]: https://gitlab.com/biomedit/sett-rs/-/merge_requests/272
  [contributed code]: https://gitlab.com/biomedit/sett-rs/-/merge_requests/278
  [made a few suggestions]: https://gitlab.com/biomedit/sett-rs/-/issues/128#note_1677977806

### SecureDrop

[SecureDrop] is a whistle-blower platform, which is used by many
organizations including The Washington Post and The Guardian.

In 2022, the SecureDrop team started [evaluating Sequoia] as an
alternate OpenPGP implementation.  There were two major motivations:
the Python library that were using to interact with GnuPG was
unmaintained, and SecureDrop had an impedance mismatch with how GnuPG
manages certificates:

> Using Sequoia as a library should allow us to use only what we
> actually need to use. We would also no longer be tied to the gpg
> on-disk keyring, so we could put them in the database.

  [SecureDrop]: https://securedrop.org/
  [evaluating Sequoia]: https://github.com/freedomofpress/securedrop/issues/6399

The SecureDrop team came up with a [migration plan].  They executed
it.  And, at the end of last year, [they requested an audit].  We
obliged.  I found [a few minor issues], which were quickly addressed.
Then in November, [SecureDrop 2.7.0] was released with Sequoia
support!

  [migration plan]: https://github.com/freedomofpress/securedrop/issues/6499
  [they requested an audit]: https://github.com/freedomofpress/securedrop/issues/6801
  [a few minor issues]: https://github.com/freedomofpress/securedrop/issues?q=is%3Aissue+author%3Anwalfield+created%3A2023-10-12+created%3A2023-10-13
  [SecureDrop 2.7.0]: https://securedrop.org/news/securedrop-2_7_0-released/

[Kunal Mehta] (legoktm), the SecureDrop developer who did most of the
work switching SecureDrop to Sequoia, wrote two blog posts about his
experience: a more official account of the process, [*Migrating
SecureDrop’s PGP backend from GnuPG to Sequoia*], and [a more personal
missive].

  [Kunal Mehta]: https://legoktm.com/
  [*Migrating SecureDrop’s PGP backend from GnuPG to Sequoia*]: https://securedrop.org/news/migrating-securedrops-pgp-backend-from-gnupg-to-sequoia/
  [a more personal missive]: https://blog.legoktm.com/2023/11/02/migrating-securedrops-pgp-backend-from-gnupg-to-sequoia.html

Among other interesting things that Kunal wrote, he said:

> We’d like to thank the Sequoia team for its work on developing such
> a useful and straightforward library

I can only reflect that back to the SecureDrop team, and Kunal, in
particular: thanks for the thoughtful interactions and feedback.  I
look forward to a continued fruitful collaboration!

### RPM Package Manager

In 2022, as part of its 4.18 release, [RPM switched to Sequoia] for
its default OpenPGP backend.  At the start of 2023, [Fedora 38 was the
first distribution to adopt the Sequoia-based backend] .

  [RPM switched to Sequoia]: https://rpm.org/wiki/Releases/4.18.0
  [Fedora 38 was the first distribution to adopt the Sequoia-based backend]: https://fedoramagazine.org/announcing-fedora-38/

As with any change in technology, there are growing pains.  And even
those differences that are intended need explanation, and have
rippling effects, which impact downstream projects and users.  Since
the Fedora 38 release, a [couple] of [issues] have been raised, but
we've stayed on top of them, and, I think, found satisfactory
solutions.

  [couple]: https://github.com/rpm-software-management/rpm-sequoia/issues/59
  [issues]: https://github.com/rpm-software-management/rpm-sequoia/pull/60

Recently, Panu removed the deprecated OpenPGP backend from RPM, and
Sequoia become RPM's only officially supported OpenPGP backend.  Panu
Matilainen, RPM's maintainer
[wrote](https://github.com/rpm-software-management/rpm/pull/2986#issuecomment-2011622754):

> Million thanks @nwalfield and @teythoon for making this possible!
>
> I can't believe just how much weight this took off my shoulders 🪶
>
> Every software engineer knows some code is more expensive to
> maintain than others, but I wonder: how do you measure it? In
> kilograms? 😄

Since Panu wrote that a month ago, I've been at a loss for words.  All
I can say is: thank you!  Collaborating with you was a great pleasure.
We had great constructive, and respectful discussions.  It's the type
of collaboration that, I think, every software engineer prefers!

# Call for Collaboration

If your project is using or thinking about using Sequoia or OpenPGP,
we'd be happy to help (even if you don't end up choosing Sequoia)!
If you are interested, feel free to [reach out to us].

  [reach out to us]: https://sequoia-pgp.org/contribute/
