---
title: "c’t Open Source Spotlight interview with Neal"
date: 2025-01-13T20:40:00+01:00
author: Neal
banner: /blog/2025/01/13/202501-ct-interview/ct-opensource-spotlight_files/14d87691-3d73-4b24-a782-59b031a5d7c6.png
---

Kurz nach dem 1.0 Release von sq, schrieb mir [Keywan
Tonekaboni](https://www.heise.de/autor/Keywan-Tonekaboni-4647374) eine
Mail und fragte mich, ob ich Zeit für ein kurzes Interview für den
[c't Open Source
Spotlight](https://www.heise.de/newsletter/anmeldung.html?id=ct-opensource)
hätte.  Ein paar Tage später erschien das Interview mit einer schönen
Einleitung von Keywan.  Leider gibt es kein Archiv also habe ich den
Inhalt mit Keywans Erlaubnis hier wiedergegeben.

<!--more-->

<br>
<table align="center" width="100%" style="background-color:GhostWhite;text-align:center;width:100%;" cellspacing="0" cellpadding="0" border="0">
  <tbody>
    <tr>
      <td align="center" valign="top" style="vertical-align:top;">
        <div align="center">
          <table align="center" width="100%" style="border:3px solid black;text-align:center;width:100%;" cellspacing="0" cellpadding="0" border="0">
            <tbody>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" class="p_16_16_16_16" valign="top">
                          <table bgcolor="#FFFFFF" width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="600" style="font-size:0;width:600px; padding-top: 75px;"><img border="0" src=
                                "ct-opensource-spotlight_files/14d87691-3d73-4b24-a782-59b031a5d7c6.png" width="600" style="display:block;" /></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" bgcolor="#FFFFFF" valign="top" style="background-color:GhostWhite;">
                  <table width="600" style="width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" bgcolor="#FFFFFF" height="32" valign="top" width="600" style="background-color:GhostWhite;line-height:0px;width:600px;"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:8px 16px 8px 16px;">
                          <table bgcolor="#FFFFFF" width="568" style="background-color:GhostWhite;width:568px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="568" style="width:568px;">
                                  <table width="568" style="width:568px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="direction:ltr;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;text-align:left;">
                                        <span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Hey, es ist Freitag und Zeit für eine neue Ausgabe
                                        Open Source Spotlight. Wie Niklas zu Nikolaus schon angekündigt hat, jetzt immer alle zwei Wochen. Bei der Gelegenheit ein herzliches
                                        Hallo an die neuen Abonnentinnen und Abonnenten unseres Newsletters. Heute stelle ich euch ein neues Tool für OpenPGP vor und habe am
                                        Ende noch einen kleinen Filmtipp. Genug der Vorrede, starten wir mit dem Spotlight! 🚀</span></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table bgcolor="#FFFFFF" width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" width="600" style="width:600px;">
                          <table width="600" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" valign="middle">
                                  <table width="200" style="background-color:GhostWhite;width:200px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="padding:16px 16px 16px 16px;">
                                          <table bgcolor="#FFFFFF" width="168" style="background-color:GhostWhite;width:168px;" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                              <tr>
                                                <td align="center" class="hauto" valign="middle" width="168" style="font-size:0;width:168px;"><img border="0" src=
                                                "ct-opensource-spotlight_files/fe4bf79b-e787-4d6a-a4db-8716acb8b7cb.png" width="168" style="display:block;" /></td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td align="left" valign="middle">
                                  <table width="400" style="background-color:GhostWhite;width:400px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="padding:8px 8px 8px 8px;">
                                          <table bgcolor="#FFFFFF" width="384" style="background-color:GhostWhite;width:384px;" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                              <tr>
                                                <td align="left" class="hauto" valign="middle" width="384" style="width:384px;">
                                                  <table width="384" style="width:384px;" cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                      <tr>
                                                        <td align="left" valign="top" style=
                                                        "direction:ltr;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;text-align:left;"><span style=
                                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><span style="font-weight:bold;">Keywan
                                                        Tonekaboni</span></span><span style=
                                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span></td>
                                                      </tr>
                                                      <tr>
                                                        <td align="left" valign="top" style=
                                                        "direction:ltr;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;text-align:left;"><span style=
                                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Redaktion c't</span><span style=
                                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span></td>
                                                      </tr>
                                                      <tr>
                                                        <td align="left" valign="top" style=
                                                        "direction:ltr;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;text-align:left;"><span style=
                                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><a href="mailto:ktn@ct.de" target="_blank" style=
                                                        "color:#357786;font-family:Arial,Helvetica,sans-serif;font-size:16px;text-decoration:underline;"><span style=
                                                        "color:#357786;font-family:Arial,Helvetica,sans-serif;font-size:16px;text-decoration:underline;">ktn@ct.de</span></a></span></td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" class="p_16_16_16_16" valign="top">
                          <table width="600" style="background-color:#154F5C;width:600px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" valign="top" style="padding:1px 1px 1px 1px;">
                                  <table width="598" style="background-color:GhostWhite;width:598px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="padding:32px 32px 32px 32px;">
                                          <table bgcolor="#FFFFFF" width="534" style="background-color:GhostWhite;width:534px;" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                              <tr>
                                                <td align="left" class="hauto" valign="top" width="534" style="width:534px;">
                                                  <table width="534" style="width:534px;" cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                      <tr>
                                                        <td align="left" valign="top" style=
                                                        "direction:ltr;font-weight:normal;line-height:36px;mso-line-height-rule:exactly;text-align:left;"><span style=
                                                        "color:#357786;font-family:Arial,Helvetica,sans-serif;font-size:24px;font-weight:bold;">Heute im
                                                        Spotlight</span><span style=
                                                        "color:#357786;font-family:Arial,Helvetica,sans-serif;font-size:24px;font-weight:bold;">&nbsp;<br /></span><span style=
                                                        "color:#357786;font-family:Arial,Helvetica,sans-serif;font-size:24px;font-weight:bold;">&nbsp;<br /></span></td>
                                                      </tr>
                                                      <tr>
                                                        <td align="left" valign="top">
                                                          <table class="wauto" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                              <tr>
                                                                <td align="left" valign="top" width="0"></td>
                                                                <td align="right" valign="top" style=
                                                                "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;">
                                                                ●</td>
                                                                <td align="left" valign="top" style=
                                                                "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;padding-left:5px;">
                                                                <span style="font-weight:bold;">Spotlight: sq 1.0 – CLI-Tool von Sequoia-PGP</span></td>
                                                              </tr>
                                                            </tbody>
                                                          </table>
                                                          <table class="wauto" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                              <tr>
                                                                <td align="left" valign="top" width="0"></td>
                                                                <td align="right" valign="top" style=
                                                                "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;">
                                                                ●</td>
                                                                <td align="left" valign="top" style=
                                                                "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;padding-left:5px;">
                                                                <span style="font-weight:bold;">Interview: Neal H. Walfield, Mitgründer von Sequoia-PGP</span></td>
                                                              </tr>
                                                            </tbody>
                                                          </table>
                                                        </td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:32px 16px 32px 16px;">
                          <table bgcolor="#FFFFFF" width="568" style="background-color:GhostWhite;width:568px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="center" class="f_0 hauto ta_c" valign="top" width="568" style="width:568px;">
                                  <table width="568" style="width:568px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" bgcolor="#F2F2F2" class="d_ib h_2 w_100" height="1" valign="top" width="568" style=
                                        "background-color:#F2F2F2;height:1px;line-height:0px;width:568px;"><img alt="" border="0" class="d" height="1" src=
                                        "ct-opensource-spotlight_files/transparent.gif" width="1" style="display:block;height:1px;width:1px;" /></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:32px 16px 8px 16px;">
                          <table bgcolor="#FFFFFF" width="568" style="background-color:GhostWhite;width:568px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="568" style="width:568px;">
                                  <table width="568" style="width:568px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="direction:ltr;font-weight:normal;line-height:36px;mso-line-height-rule:exactly;text-align:left;">
                                        <span style="color:#357786;font-family:Arial,Helvetica,sans-serif;font-size:24px;font-weight:bold;">sq 1.0 – CLI-Tool von
                                        Sequoia-PGP</span></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:8px 16px 8px 16px;">
                          <table bgcolor="#FFFFFF" width="568" style="background-color:GhostWhite;width:568px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="568" style="width:568px;">
                                  <table width="568" style="width:568px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="direction:ltr;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;text-align:left;">
                                        <span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">PGP (Pretty Good Privacy) kennen die meisten von euch
                                        vermutlich von E-Mail-Verschlüsselung. Zugegeben, es ist nicht das eingängigste und benutzerfreundlichste Konzept und viele Leute
                                        benutzen es nicht. Auch die meisten meiner Mails schicke ich unverschlüsselt raus, selbst wenn mein Gegenüber PGP nutzt. Doch
                                        Hand-aufs-Herz: die Alternativen haben auch ihre Probleme und Abseits von Messengern (sprich Signal &amp; Co.) habe ich auch keine
                                        brauchbaren Konzepte für sichere Massenverschlüsselung gesehen.</span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Was wir nicht vergessen dürfen: Für Menschen in repressiven
                                        Staaten sind ein paar User-Experience-Unannehmlichkeiten vermutlich zu verschmerzen, wenn dafür die Vertraulichkeit ihrer Kommunikation
                                        gewahrt bleibt. Und außerdem gibt es für PGP Use-Cases abseits von Mail-Verschlüsselung, etwa das Signieren von Softwarepaketen, damit
                                        bei der Installation von Programmen oder einem Systemupdate einem keine Malware und kein Staatstrojaner untergejubelt wird. Das nutzen
                                        unter Linux etwa die Paketmanager und Update-Tools von Debian und Ubuntu (apt), Fedora (dnf) oder openSUSE (zypper).</span> <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Die bekannteste PGP-Implementierung ist vermutlich GnuPG
                                        beziehungsweise gpg. Es gibt aber auch andere, und eine davon ist Sequoia PGP. Drei ehemalige GnuPG-Entwickler, namentlich Neal Walfield,
                                        Justus Winter und Kai Michaelis, starteten 2017 das Projekt, um von Grund auf und in Rust OpenPGP neu zu implementieren. Die Hintergründe
                                        dazu verrät Neal weiter unten im Interview. Diese Woche hat das Projekt nun</span> <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><a href=
                                        "https://sequoia-pgp.org/blog/2024/12/16/202412-sq-1.0/" target="_blank" style=
                                        "color:#357786;font-family:Arial,Helvetica,sans-serif;font-size:16px;text-decoration:underline;">Version 1.0 ihres Kommandozeilen-Tools
                                        <span style="font-weight:bold;">sq</span></a></span> <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">vorgestellt.</span></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:8px 16px 8px 16px;">
                          <table bgcolor="#FFFFFF" width="568" style="background-color:GhostWhite;width:568px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="568" style="width:568px;">
                                  <table width="568" style="width:568px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="direction:ltr;font-weight:normal;line-height:24px;mso-line-height-rule:exactly;text-align:left;">
                                        <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:bold;">Spotlight-Steckbrief</span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:bold;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:bold;">&nbsp;<br /></span></td>
                                      </tr>
                                      <tr>
                                        <td align="left" valign="top">
                                          <table class="wauto" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                              <tr>
                                                <td align="left" valign="top" width="0"></td>
                                                <td align="right" valign="top" style=
                                                "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;">
                                                ●</td>
                                                <td align="left" valign="top" style=
                                                "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;padding-left:5px;">
                                                <span style="font-weight:bold;">Name: Sequoia PGP</span></td>
                                              </tr>
                                            </tbody>
                                          </table>
                                          <table class="wauto" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                              <tr>
                                                <td align="left" valign="top" width="0"></td>
                                                <td align="right" valign="top" style=
                                                "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;">
                                                ●</td>
                                                <td align="left" valign="top" style=
                                                "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;padding-left:5px;">
                                                <span style="font-weight:bold;">GitLab:</span> <a href=
                                                "https://gitlab.com/sequoia-pgp/sequoia-sq" target="_blank" style=
                                                "color:#357786;font-family:Arial,Helvetica,sans-serif;font-size:16px;text-decoration:underline;">https://gitlab.com/sequoia-pgp/sequoia-sq</a></td>
                                              </tr>
                                            </tbody>
                                          </table>
                                          <table class="wauto" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                              <tr>
                                                <td align="left" valign="top" width="0"></td>
                                                <td align="right" valign="top" style=
                                                "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;">
                                                ●</td>
                                                <td align="left" valign="top" style=
                                                "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;padding-left:5px;">
                                                <span style="font-weight:bold;">Webseite:</span> <a href=
                                                "https://sequoia-pgp.org/" target="_blank" style=
                                                "color:#357786;font-family:Arial,Helvetica,sans-serif;font-size:16px;text-decoration:underline;">https://sequoia-pgp.org/</a></td>
                                              </tr>
                                            </tbody>
                                          </table>
                                          <table class="wauto" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                              <tr>
                                                <td align="left" valign="top" width="0"></td>
                                                <td align="right" valign="top" style=
                                                "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;">
                                                ●</td>
                                                <td align="left" valign="top" style=
                                                "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;padding-left:5px;">
                                                <span style="font-weight:bold;">Entwickler:</span> Neal H. Walfield, Justus Winter und das Sequoia Team</td>
                                              </tr>
                                            </tbody>
                                          </table>
                                          <table class="wauto" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                              <tr>
                                                <td align="left" valign="top" width="0"></td>
                                                <td align="right" valign="top" style=
                                                "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;">
                                                ●</td>
                                                <td align="left" valign="top" style=
                                                "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;padding-left:5px;">
                                                <span style="font-weight:bold;">Plattform:</span> Linux, macOS, Windows</td>
                                              </tr>
                                            </tbody>
                                          </table>
                                          <table class="wauto" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                              <tr>
                                                <td align="left" valign="top" width="0"></td>
                                                <td align="right" valign="top" style=
                                                "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;">
                                                ●</td>
                                                <td align="left" valign="top" style=
                                                "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;padding-left:5px;">
                                                <span style="font-weight:bold;">Lizenz:</span> LGPLv2+</td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td align="left" valign="top" style="direction:ltr;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;text-align:left;">
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:8px 16px 8px 16px;">
                          <table bgcolor="#FFFFFF" width="568" style="background-color:GhostWhite;width:568px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="568" style="width:568px;">
                                  <table width="568" style="width:568px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="direction:ltr;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;text-align:left;">
                                        <span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Was mir gleich an</span> <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><span style="font-style:italic;">sq</span></span> <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">auffällt: statt vieler Doppelbindestrich-Schalter (--decrypt)
                                        nutzt es für die wichtigsten Funktionen Subkommandos, die sprachlich auch recht verständlich sind.</span> <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:16px 0px 16px 0px;">
                          <table bgcolor="#FFFFFF" width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="center" class="hauto" valign="middle" width="600" style="font-size:0;width:600px;"><img alt="Screenshot Ausgabe Befehl sq" border="0"
                                src="ct-opensource-spotlight_files/8e69ae4e-b47e-46d8-9c52-53818506c317.png" width="465" style="display:block;" /></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:0px 16px 8px 16px;">
                          <table bgcolor="#FFFFFF" width="568" style="background-color:GhostWhite;width:568px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="568" style="width:568px;">
                                  <table width="568" style="width:568px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="direction:ltr;font-weight:normal;line-height:18px;mso-line-height-rule:exactly;text-align:left;">
                                        <span style="color:#357786;font-family:Arial,Helvetica,sans-serif;font-size:12px;"><span style="color:#323232;">Das Kommandozeilentool sq
                                        arbeitet mit nachvollziehbaren Befehlen statt mit kryptischen Argumenten. (Screenshot: ktn/c’t)</span></span></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:8px 16px 8px 16px;">
                          <table bgcolor="#FFFFFF" width="568" style="background-color:GhostWhite;width:568px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="568" style="width:568px;">
                                  <table width="568" style="width:568px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="direction:ltr;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;text-align:left;">
                                        <span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Wollt ihr meinen PGP-Key online suchen und
                                        importieren, dann sieht mit</span> <span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><span style=
                                        "font-style:italic;">sq</span></span> <span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">der Befehl so
                                        aus:</span></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:8px 0px 8px 0px;">
                          <table width="600" style="background-color:#323232;width:600px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" valign="top" style="padding:32px 32px 32px 32px;">
                                  <table bgcolor="#323232" width="536" style="background-color:#323232;width:536px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" class="hauto" valign="top" width="536" style="width:536px;">
                                          <table width="536" style="width:536px;" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                              <tr>
                                                <td align="left" valign="top" style="direction:ltr;font-weight:normal;text-align:left;"><span style=
                                                "color:#FFFFFF;font-family:'Courier New',Courier,monospace;font-size:16px;"><span style="color:#FFFFFF;">sq network search
                                                ktn@ct.de</span></span></td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:8px 16px 8px 16px;">
                          <table bgcolor="#FFFFFF" width="568" style="background-color:GhostWhite;width:568px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="568" style="width:568px;">
                                  <table width="568" style="width:568px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="direction:ltr;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;text-align:left;">
                                        <span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Für Verwirrung bei vielen PGP-Neulingen sorgt vor
                                        allem die Unterscheidung zwischen privaten Schlüsseln, die man nicht herausrücken darf, und öffentlichen Schlüsseln, die man unter die
                                        Leute bringen muss. Dem begegnet das Sequoia-Team sprachlich, indem Schlüssel (keys) nur die eigenen … ähh … PGP-Dinger sind, die ein
                                        Geheimnis enthalten. Die öffentlichen PGP-Gegenstücke heißen in der Sequoia Nomenklatur Zertifikate (certs).</span> <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Deshalb zeigt der folgende Aufruf …</span></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:8px 0px 8px 0px;">
                          <table width="600" style="background-color:#323232;width:600px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" valign="top" style="padding:32px 32px 32px 32px;">
                                  <table bgcolor="#323232" width="536" style="background-color:#323232;width:536px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" class="hauto" valign="top" width="536" style="width:536px;">
                                          <table width="536" style="width:536px;" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                              <tr>
                                                <td align="left" valign="top" style="direction:ltr;font-weight:normal;text-align:left;"><span style=
                                                "color:#FFFFFF;font-family:'Courier New',Courier,monospace;font-size:16px;"><span style="color:#FFFFFF;">sq key
                                                list</span></span></td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:8px 16px 8px 16px;">
                          <table bgcolor="#FFFFFF" width="568" style="background-color:GhostWhite;width:568px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="568" style="width:568px;">
                                  <table width="568" style="width:568px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="direction:ltr;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;text-align:left;">
                                        <span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">… auch nur eure eigenen Schlüssel an, während der
                                        folgende Befehl auch die Daten von Dritten in eurem PGP-Speicher zeigt (Ihr seht, ich versuche mich, um den Begriff Schlüssel zu
                                        drücken).</span></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:8px 0px 8px 0px;">
                          <table width="600" style="background-color:#323232;width:600px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" valign="top" style="padding:32px 32px 32px 32px;">
                                  <table bgcolor="#323232" width="536" style="background-color:#323232;width:536px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" class="hauto" valign="top" width="536" style="width:536px;">
                                          <table width="536" style="width:536px;" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                              <tr>
                                                <td align="left" valign="top" style="direction:ltr;font-weight:normal;text-align:left;"><span style=
                                                "color:#FFFFFF;font-family:'Courier New',Courier,monospace;font-size:16px;"><span style="color:#FFFFFF;">sq cert
                                                list</span></span></td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:8px 16px 8px 16px;">
                          <table bgcolor="#FFFFFF" width="568" style="background-color:GhostWhite;width:568px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="568" style="width:568px;">
                                  <table width="568" style="width:568px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="direction:ltr;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;text-align:left;">
                                        <span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Aus meiner Sicht ist das neue Vokabular tatsächlich
                                        verständlicher: Mit einem Schlüssel schließe ich meine Sachen auf; ein Zertifikat lege ich vor oder bekomme es vorgelegt. Okay, die
                                        Metapher für Signieren (mit Schlüssel) und für andere Personen Verschlüsseln (mit deren Zertifikat) hinkt etwas, aber doch einfacher zu
                                        unterscheiden als private und öffentliche Schlüssel.</span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Wenn ihr Sequoia ausprobieren wollt, findet ihr derzeit ältere
                                        Versionen von</span> <span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><span style=
                                        "font-style:italic;">sq</span></span> <span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">in diversen
                                        Linux-Distribitonen. Bei Arch Linux, Fedora und openSUSE heißt das Paket sequoia-sq, während es bei Debian und Ubuntu schlicht unter sq
                                        paketiert ist. In Ubuntu und Debian Testing/Unstable findet ihr auch das</span> <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><a href="https://gitlab.com/sequoia-pgp/sequoia-chameleon-gnupg"
                                        style= "color:#357786;font-family:Arial,Helvetica,sans-serif;font-size:16px;text-decoration:underline;">„Chamäleon“
                                        gpg-from-sq</a></span><span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">, ein Nachbau von gpg mit
                                        Sequoia. Das ist nützlich, falls ihr noch mit der neuen Syntax hadert oder (grafische) Tools verwendet, die gpg erwarten.</span>
                                        <span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Falls ihr sq 1.0 selbst ausprobieren wollt, könnt ihr das mit Rust
                                        cargo selbst bauen. Unter Fedora habe ich zunächst die benötigten Abhängigkeiten installiert und dann Cargo aufgerufen:</span></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:8px 0px 8px 0px;">
                          <table width="600" style="background-color:#323232;width:600px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" valign="top" style="padding:32px 32px 32px 32px;">
                                  <table bgcolor="#323232" width="536" style="background-color:#323232;width:536px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" class="hauto" valign="top" width="536" style="width:536px;">
                                          <table width="536" style="width:536px;" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                              <tr>
                                                <td align="left" valign="top" style="direction:ltr;font-weight:normal;text-align:left;"><span style=
                                                "color:#FFFFFF;font-family:'Courier New',Courier,monospace;font-size:16px;"><span style="color:#FFFFFF;">dnf install cargo clang
                                                git nettle-devel openssl-devel capnproto libsqlite3x-devel</span></span><span style=
                                                "color:#FFFFFF;font-family:'Courier New',Courier,monospace;font-size:16px;">&nbsp;<br /></span><span style=
                                                "color:#FFFFFF;font-family:'Courier New',Courier,monospace;font-size:16px;"><span style="color:#FFFFFF;">cargo install sequoia-sq
                                                --locked</span></span></td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:32px 16px 8px 16px;">
                          <table bgcolor="#FFFFFF" width="568" style="background-color:GhostWhite;width:568px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="568" style="width:568px;">
                                  <table width="568" style="width:568px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="direction:ltr;font-weight:normal;line-height:36px;mso-line-height-rule:exactly;text-align:left;">
                                        <span style="color:#357786;font-family:Arial,Helvetica,sans-serif;font-size:24px;font-weight:bold;">Interview: Neal H. Walfield,
                                        Mitgründer von Sequoia-PGP</span></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:8px 16px 8px 16px;">
                          <table bgcolor="#FFFFFF" width="568" style="background-color:GhostWhite;width:568px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="568" style="width:568px;">
                                  <table width="568" style="width:568px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="direction:ltr;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;text-align:left;">
                                        <span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Schon in seiner Doktorarbeit beschäftigte sich Neal
                                        Walfield damit, wie man das Nutzererlebnis auf Mobilgeräten verbessern kann, ohne den Datenschutz zu kompromittieren. Ich habe ihn
                                        anlässlich des Releases von sq 1.0 für euch interviewt.</span> <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><span style="font-weight:bold;">c’t: Wie kamst du dazu, Software
                                        zu entwickeln?</span></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><span style="font-weight:bold;">Neal:</span></span> <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Den ersten Rechner bekamen wir 1993, als ich 13 Jahre alt war.
                                        Darauf lief MS-DOS. Meine Eltern kauften uns Kindern ein paar Spiele. Ich fand es viel interessanter, sie zu installieren und die Dateien
                                        anzusehen, als zu spielen. Nach einiger Zeit entdeckte ich Q-Basic und das mitgelieferte Spiel Nibbles. Ich sah mir den Quellcode genau
                                        an und veränderte ihn. Irgendwann verstand ich genug, dass ich meine eigenen Programme entwickeln konnte, und das tat (und tue!) ich mit
                                        Leidenschaft.</span> <span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><span style="font-weight:bold;">c’t: Was war eure Motivation,
                                        Sequoia PGP und sq zu entwickeln?</span></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><span style="font-weight:bold;">Neal:</span></span> <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Sequoia ist für mich in erster Linie ein Menschenrechtsprojekt.
                                        Menschenrechte sind unverhandelbar. Allerdings scheinen nicht alle, besonders die, die Macht haben, das verstanden zu haben. Daher setzte
                                        ich mich im Rahmen meiner Möglichkeiten und Kenntnisse dafür ein.</span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Da ich ein relativ guter Programmierer bin und Spaß daran habe,
                                        versuche ich vor allem in diesem Bereich etwas Gutes zu tun. Ich glaube nicht, dass Technologie soziale Probleme lösen kann; dafür
                                        brauchen wir soziale Lösungen. Aber ich denke, dass Technologie einen großen Einfluss auf unsere Gestaltungsmöglichkeiten haben kann,
                                        sowohl positiv als auch negativ.</span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Zu Sequoia kam ich über GnuPG. Am Ende meiner Promotion habe ich
                                        nach einem Job gesucht. Das war 2013, zur Zeit der Snowden-Enthüllungen. Ich war damals schon lange mit Werner Koch, dem Gründer von
                                        GnuPG, befreundet und durch die Aufmerksamkeit bekam er auch mehr Geld. Er stellte mich ein.</span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Ich arbeitete direkt an GnuPG, kollaborierte mit
                                        Anwendungsentwicklern und sprach mit vielen Endnutzern. Mir wurde klar, wie wichtig das Projekt war. Gleichzeitig gab es auch einige
                                        Unzufriedenheiten. Ich hatte Ideen, wie man GnuPG verbessern könnte. Werner war aber einer anderen Meinung. Da ich ihn nicht zwingen
                                        wollte, in eine andere Richtung zu gehen, entschlossen sich ein paar andere und ich zu kündigen. Weil wir viel Potenzial sahen und
                                        überzeugt waren, dass das OpenPGP-Protokoll ziemlich gut ist, beschlossen wir, Sequoia zu gründen.</span></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:16px 0px 16px 0px;">
                          <table bgcolor="#FFFFFF" width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="center" class="hauto" valign="middle" width="600" style="font-size:0;width:600px;"><img alt=
                                "Selfie von Neal und Justus vor dem Brandenburger Tor. Beide mit einem breiten Grinsen." border="0" src=
                                "ct-opensource-spotlight_files/aaedb7a4-01c9-43ee-8d25-e2e9800553c8.jpg" width="499" style="display:block;" /></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:0px 16px 8px 16px;">
                          <table bgcolor="#FFFFFF" width="568" style="background-color:GhostWhite;width:568px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="568" style="width:568px;">
                                  <table width="568" style="width:568px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="direction:ltr;font-weight:normal;line-height:18px;mso-line-height-rule:exactly;text-align:left;">
                                        <span style="color:#357786;font-family:Arial,Helvetica,sans-serif;font-size:12px;"><span style="color:#323232;">Die beiden Sequoia-PGP
                                        Hauptentwickler Neal Walfield und Justus Winter vor dem Brandenburger Tor. (Credit: Neal Walfield)</span></span></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:8px 16px 8px 16px;">
                          <table bgcolor="#FFFFFF" width="568" style="background-color:GhostWhite;width:568px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="568" style="width:568px;">
                                  <table width="568" style="width:568px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="direction:ltr;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;text-align:left;">
                                        <span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><span style="font-weight:bold;">c’t: Welches Problem
                                        löst Sequoia PGP und wofür braucht es sq?</span></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><span style="font-weight:bold;">Neal:</span></span> <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Sequoia dreht sich um Informationssicherheit für Menschen. Wir
                                        versuchen, benutzerfreundliche, robuste und sichere Werkzeuge bereitzustellen, die den Menschen dienen. Das bedeutet vor allem dezentrale
                                        Lösungen.</span><span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Bei der Entwicklung von Sequoia folgen wir dem Zwiebel-Prinzip:
                                        Wir fangen innen an und bauen eine solide Schicht nach der anderen auf. So haben wir mit unserer low-level OpenPGP Bibliothek
                                        sequoia-openpgp begonnen. Version 1.0 haben wir vor vier Jahren veröffentlicht. Bisher wurde sequoia-openpgp in SecureDrop, den
                                        Linux-Paketmanager RPM, Sett (einem Projekt der Schweizer Regierung zum Schutz von Patientendaten) und einigen anderen Projekten
                                        integriert.</span><span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Seitdem arbeiten wir an der nächsten Schicht. Wir bauen
                                        verschiedene Dienste, die von vielen Anwendungen benötigt werden, wie zum Beispiel einen geteilten Zertifikatsspeicher. Um uns auf die
                                        Bedürfnisse der Menschen zu fokussieren, haben wir gleichzeitig sq, ein Kommandozeilentool, entwickelt. sq richtet sich an Benutzer, die
                                        gerne auf der Kommandozeile arbeiten. Aber wir haben uns bemüht, die Konzepte so zu entwickeln, dass sie sich leicht auf ein GUI
                                        übertragen lassen. Ein wichtiges Konzept von Sequoia ist etwa die Authentifizierung. Ohne Authentifizierung sind Verschlüsselung und
                                        Signaturprüfung viel schwächer. sq kann nicht nur anzeigen, ob ein Zertifikat zu einer Entität gehört, sondern auch, wie es zu dieser
                                        Schlussfolgerung gekommen ist. Diese Form der Transparenz ist extrem wichtig, damit Menschen verstehen, warum ein System funktioniert.
                                        Nur so kann Vertrauen aufgebaut werden.</span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><span style="font-weight:bold;">c’t: Was waren technische Hürden,
                                        mit denen ihr bei der Entwicklung von sq konfrontiert wart?</span></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><span style="font-weight:bold;">Neal:</span></span> <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Die größten Hürden sind meiner Meinung nach nicht technischer,
                                        sondern menschlicher Natur. Ein Computer beherrscht Kryptografie sehr gut und sehr schnell. Aber die Sicherheitsentscheidungen muss ein
                                        Mensch treffen. Unser Ziel in sq ist es, Menschen zu helfen, diese Entscheidungen zu treffen beziehungsweise sicher zu delegieren, indem
                                        wir es ihnen so bequem wie möglich machen, und zwar im Rahmen ihres Angriffsmodells. Um das zu konkretisieren: globale CAs, wie sie zum
                                        Beispiel im Web bei TLS zum Einsatz kommen, haben andere Interessen (wirtschaftlich, politisch, etc.) als deren Nutzer. sq macht es
                                        genauso einfach, dem Techie aus einem Aktivistenkollektiv zu vertrauen, wie einer globalen CA.</span></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:16px 0px 16px 0px;">
                          <table bgcolor="#FFFFFF" width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="center" class="hauto" valign="middle" width="600" style="font-size:0;width:600px;"><img alt=
                                "Foto von Neal mit Mikro im Vortragssal, Rollup und Tisch mit FOSDEM Logo. Laptop mit vielen Aufklebern, CYBER darf dabei nicht fehlen." border=
                                "0" src="ct-opensource-spotlight_files/071df388-19b6-48ec-b3e2-373dde0e3cc6.jpg" width="499" style="display:block;" /></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:0px 16px 8px 16px;">
                          <table bgcolor="#FFFFFF" width="568" style="background-color:GhostWhite;width:568px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="568" style="width:568px;">
                                  <table width="568" style="width:568px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="direction:ltr;font-weight:normal;line-height:18px;mso-line-height-rule:exactly;text-align:left;">
                                        <span style="color:#357786;font-family:Arial,Helvetica,sans-serif;font-size:12px;"><span style="color:#323232;">Neal bei einem Vortrag
                                        über Sequoia PGP auf der FOSDEM. (Credit: Neal Walfield)</span></span></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:8px 16px 8px 16px;">
                          <table bgcolor="#FFFFFF" width="568" style="background-color:GhostWhite;width:568px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="568" style="width:568px;">
                                  <table width="568" style="width:568px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="direction:ltr;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;text-align:left;">
                                        <span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><span style="font-weight:bold;">c’t: Welche Pläne habt
                                        ihr als Nächstes für Sequoia PGP und sq?</span></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Neal: Entsprechend dem oben erwähnten Zwiebel-Prinzip arbeiten wir
                                        weiter an den äußeren Zwiebelschalen in Richtung Nutzer. Diese Zwiebelschalen sind: eine high-level Bibliothek, mehr Werkzeuge, um
                                        beispielsweise ein CA leichter aufzustellen, mehr Integrationen in bestehende Tools, Werkzeuge, um die Supply Chain Security zu
                                        verbessern (hier haben wir schon mit sequoia-git angefangen), und auch GUI-Anwendungen.</span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><span style="font-weight:bold;">c’t: Wofür begeistert du dich
                                        abseits von Softwareentwicklung?</span></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Neal: Meine Freizeit verbringe ich meist mit meinen Kindern. In
                                        der Corona Zeit habe ich angefangen, Brot zu backen. Die alten Weizensorten wie Khorasan finde ich besonders lecker. Ich lese auch gerne.
                                        Neulich habe ich die Kinderbücher von Peter Härtling entdeckt.</span></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:8px 16px 8px 16px;">
                          <table bgcolor="#FFFFFF" width="568" style="background-color:GhostWhite;width:568px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="568" style="width:568px;">
                                  <table width="568" style="width:568px;" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td align="left" valign="top" style="direction:ltr;font-weight:normal;line-height:29px;mso-line-height-rule:exactly;text-align:left;">
                                        <span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Oh, Peter Härtlings „Ben liebt Anna“ habe ich in der
                                        Schule gelesen und es hat mir etwas Mut gemacht, meinen Schwarm anzusprechen. ☺️ Und wo wir gerade Kinderbücher sind. Bereits vor einigen
                                        Jahren hat Matthias Kirschner, Präsident der Free Software Foundation Europe (FSFE) mit der Illustratorin Sandra Brandstätter das
                                        Kinderbuch „Ada &amp; Zangemann“ veröffentlicht. Jetzt gibt es das „Ein Märchen über Software, Skateboards und Himbeereis“ auch als einen
                                        animierten Film, der die schönen Illustrationen in Bewegung bringt. Den halbstündigen Film könnt ihr kostenlos im Fediverse bei der FSFE
                                        abrufen:</span><span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><a href="https://pics.fsfe.org/gallery/5Tcc_rbuFkkT5FWB9xd3jSAK" style=
                                        "color:#357786;font-family:Arial,Helvetica,sans-serif;font-size:16px;text-decoration:underline;">https://pics.fsfe.org/gallery/5Tcc_rbuFkkT5FWB9xd3jSAK</a></span><span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br />
                                        </span><span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;</span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Wie immer freuen wir uns über Feedback. Wie haltet ihr es mit PGP?
                                        Lasst es mich gerne wissen, indem ihr unten auf "E-Mail schreiben" klickt. Damit verabschiede ich mich und packe meine Tasche für
                                        den</span> <span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><a
                                        href="https://events.ccc.de/congress/2024/infos/startpage.html" target="_blank" style=
                                        "color:#357786;font-family:Arial,Helvetica,sans-serif;font-size:16px;text-decoration:underline;">Chaos Communication Congress - kurz
                                        38C3</a></span><span style="color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">. Wenn ihr auch dort seid oder dort
                                        spannende Projekte kennt, bei denen ich vorbeischauen sollte, schreibt eine Mail an</span> <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><a href="mailto:ktn@ct.de" target="_blank" style=
                                        "color:#357786;font-family:Arial,Helvetica,sans-serif;font-size:16px;text-decoration:underline;">ktn@ct.de</a></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">, auf</span> <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><a href="https://social.heise.de/@ktn"" target="_blank" style=
                                        "color:#357786;font-family:Arial,Helvetica,sans-serif;font-size:16px;text-decoration:underline;">Mastodon</a></span> <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">oder ruft mich per DECT:5828 an. Ich melde mich dann mit der
                                        nächsten Ausgabe von Open Source Spotlight wieder am 3. Januar.</span> <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Viel Spaß am Gerät und unterm Weihnachtsbaum. 🎄</span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">&nbsp;<br /></span><span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;">Bis zum nächsten Spotlight und</span> <span style=
                                        "color:#323232;font-family:Arial,Helvetica,sans-serif;font-size:16px;"><span style="font-weight:bold;">Happy Hacking!</span></span></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top">
                  <table width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="padding:16px 0px 16px 0px;">
                          <table bgcolor="#FFFFFF" width="600" style="background-color:GhostWhite;width:600px;" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td align="left" class="hauto" valign="top" width="600" style="font-size:0;width:600px; padding-bottom: 75px;"><img border="0" src=
                                "ct-opensource-spotlight_files/7c5d791f-a9b3-4734-953f-1680e38511cb.jpg" width="600" style="display:block;" /></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </td>
    </tr>
  </tbody>
</table>
