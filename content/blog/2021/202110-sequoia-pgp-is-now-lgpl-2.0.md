---
title: "Sequoia PGP is now LGPL 2.0+"
author: Neal
date: 2021-10-18T16:55:00+02:00
banner: img/lgplv3-with-text.png
---

We're happy to announce that we've changed Sequoia PGP's license from
the GPL 2+ to the more permissive LGPL 2+.  Simultaneously, we've also
released version 1.5 of the [openpgp crate] under these terms.

  [openpgp crate]: https://crates.io/crates/sequoia-openpgp

<!--more-->

<div style="width: 33%; padding: 1em; float: right;">
  <img src="/img/lgplv3-with-text.png"
       style="width:100%"
       alt="LGPL v3: Free Software; Free as in Freedom.">
</div>

When we started Sequoia, we choose to license the code under the GPL
2+.  One reason that we chose the GPL is that it makes a political
statement: we support free software.

Over the past four years, however, several free software projects have
chosen not to use Sequoia, because it is under the GPL.  [Delta Chat]
planned an iOS app, but because [Apple does not allow GPL software] in
their App store, the Delta Chat developers couldn't use Sequoia.  And,
when [Thunderbird looked for a new OpenPGP library, they rejected
Sequoia] because it was licensed under the GPL.  These are the two
most prominent examples of free software projects rejecting Sequoia.
Unfortunately, there are others.

  [Delta Chat]: https://delta.chat/
  [Apple does not allow GPL software]: https://www.fsf.org/blogs/licensing/more-about-the-app-store-gpl-enforcement
  [Thunderbird looked for a new OpenPGP library, they rejected Sequoia]: https://groups.google.com/g/tb-planning/c/7fQN0hdIvLg/m/0urCywlNBwAJ

Given these results, we have concluded that our strategy was a
mistake.  Choosing the GPL prevented not only proprietary products
from using Sequoia, but also free software projects.  That's not what
we wanted.

Today, we unanimously decided with our partners at the [p≡p
foundation] and [p≡p security] to change Sequoia's license to the LGPL
2.0+.  We hope this change will strengthen the free software
ecosystem.

  [p≡p foundation]: https://pep.foundation/
  [p≡p security]: https://web.archive.org/web/20230408031952/https://www.pep.security/

### New API in Version 1.5

Version 1.5 does not introduce any new API.

## Financial Support

Currently, the [p≡p foundation] pays six people to work on Sequoia.
Since the start of the project four years ago, p≡p has covered nearly
all of the development costs.

  [p≡p foundation]: https://pep.foundation/

We are actively looking for [additional financial support] to
diversify our funding.

You don't need to directly use Sequoia to be positively impacted by
it.  For instance, [OpenPGP CA] is a new tool for creating federated
CAs targeted at simplifying the use of OpenPGP for activists, lawyers,
and journalists who can't rely on centralized authentication
solutions.  So, [consider donating].  Of course, if your company is
using Sequoia, consider sponsoring a developer (or two).  Note: if you
want to use Sequoia under a license other than the LGPLv2+, please
[contact the foundation].

  [additional financial support]: https://pep.foundation/support-pEp/
  [OpenPGP CA]: https://openpgp-ca.org/
  [consider donating]: https://pep.foundation/support-pEp/
  [contact the foundation]: https://pep.foundation/contact/
