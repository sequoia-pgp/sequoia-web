---
title: "The Future of Sequoia PGP"
author: Neal
date: 2021-12-13T14:26:00+01:00
banner: img/sequoia-banner.jpeg
---

NLnet recently held a webinar on the future of OpenPGP.  The Sequoia
team made five short presentations.  In addition to an introduction
summarizing the past, present, and future of Sequoia, we presented
four of our current projects, which provide a nice cross section of
our current work.

<!--more-->

On November 23rd, [NLnet] hosted a [webinar on the future of OpenPGP].
There were eight presentations, including five from the Sequoia team:

  - Sequoia PGP - Neal H. Walfield
  - Adding TPM Support to Sequoia PGP - Wiktor Kwapisiewicz
  - Making sq better - Lars Wirzenius
  - Sequoia’s GnuPG Chameleon - Justus Winter
  - OpenPGP CA - a certification authority for trust management in groups - Heiko Schäfer

A complete recording of the webinar including an introduction to NGI
Assure, the eight presentations, and the Q&A session is available on
[conf.tube].  The following embedded video is a shorter version, which
we've edited to only include the five Sequoia-related presentations,
and has subtitles.  For those who prefer prose to a presentation, this
blog post summarizes the presentations' main points.

  [NLnet]: https://nlnet.nl/
  [webinar on the future of OpenPGP]: https://nlnet.nl/events/20211123/PGP/index.html
  [conf.tube]: https://conf.tube/videos/watch/4c6266f1-eefe-4f4b-8f54-9d31e5130080?start=0s

<center>
<script type="text/javascript">
<!--
  window.onload = function() {
    document.getElementById('video-player')
      .addEventListener('click', function (e) {
        var span = document.getElementById('video-player');
        span.innerHTML = '<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/RV1E_DjhCX0?start=56&autoplay=1&cc_load_policy=1" title="YouTube video player" frameborder="0" allowfullscreen></iframe>';
      });
  };
 -->
</script>
  <span id="video-player">
    <b>Clicking on the thumbnail will load content from YouTube.</b>
    <br>
    <img width="560" height="315" style="padding: 0em"
      src="/img/blog/202112-future-of-sequoia-pgp/nlnet-presentation-thumbnail.png"
      alt="Thumbnail from the NLnet presentations"
      title="Clicking will load content from YouTube.">
  </span>
</center>

## Introduction

I, Neal, started off the round of presentations.  I talked about the
history of Sequoia, and how the [p≡p foundation] helped Justus, Kai
and I found the project in 2017, and continues to be our home and main
financial supporter.  I presented our goals, what we've done over the
past 4 and a half years, what we're working on now, and our vision for
the future.

  [p≡p foundation]: https://pep.foundation

The short version of my talk is: we're not just writing a new OpenPGP
implementation.  We're trying to improve the whole internet freedom
tooling ecosystem.  We're focusing on OpenPGP, because we think that
it provides a great decentralized [PKI], and simple, yet powerful
authentication mechanisms both of which are essential to strong
end-to-end encrypted communication, but are largely ignored by state
of the art end-to-end encryption solutions.  And the good thing about
those mechanisms is that they can be used not only to authenticate
OpenPGP messages, but also by `ssh` to authenticate clients and
servers, Matrix to authentication conversation partners, [age] to
authenticate recipients, etc.  It's true that OpenPGP's web of trust
is hard to use today, but we think new and improved tooling like
[OpenPGP CA] will make the web of trust practical for many users,
especially those at risk.

  [PKI]: https://en.wikipedia.org/wiki/Public_key_infrastructure
  [age]: https://github.com/FiloSottile/age
  [OpenPGP CA]: https://openpgp-ca.org/

## TPM Support

Wiktor then talked about his work on adding TPM support to Sequoia.  A
[TPM] (Trusted Platform Module) is a coprocessor, which holds
cryptographic keys, and provides an API to the host to sign and
decrypt data.  The advantage of using a TPM instead of storing private
key material on the file system is that an attacker cannot ex-filtrate
the private key material.  This makes TPMs similar to smart cards,
like the [Nitrokey] and other OpenPGP cards.  The crucial difference
is that with a TPM there is nothing else to buy: TPMs are included in
nearly all consumer laptops that are sold today.

  [TPM]: https://en.wikipedia.org/wiki/Trusted_Platform_Module
  [Nitrokey]: https://www.nitrokey.com/

In addition to supporting signing and decryption, Wiktor is also
working on secure key migration.  The problem that key migration
solves is how to transfer a private key from a secure place like an
offline air gapped system through an online system such as an
internet-connected laptop to the secure chip.  The TPM specification
achieves this through the use of a storage key that is used to wrap
the private keys in such a way that they can only be decrypted inside
the TPM.

## Improving `sq`

Lars presented his work on improving `sq`, our command line tool.
`sq` uses a subcommand-style interface, similar to CVS and git, which,
we and [others] think, makes it easier to use.

  [others]: https://lobste.rs/s/kcfcsc/next_generation_internet_webinar_on#c_igm8u0

Lars' project consists of three parts.  First, he is adding missing
*and* useful functionality to `sq`.  To identify what we should add,
he's surveying gpg's functionality, and he's interviewing
stakeholders.  The goal is not to implement every feature that GnuPG
has, but to support the features that people actually need on a
day-to-day basis, and handle esoteric use cases by deferring to the
library.

Because programmatic use of `sq` is going to happen whether we want it
to or not (we'd rather developers use our library), the second part of
Lars' project is to add a programmatic interface to `sq`.  `sq` is
already designed to be RESTful in the sense that commands are
sessionless and don't require the user to navigate a state machine.
The focus of this effort is to add an alternative output format, which
doesn't require programs to parse `sq`'s output in an ad-hoc manner.
For this, we decided to use JSON due to its broad support by basically
every programming language.  After we add JSON support, we can easily
add other formats, like Emacs' s-expressions.  Here's the basic idea:

```
$ sq inspect --format=json foo.pgp
{
"_sq_schema": [1,0,0],
"filename": "foo.pgp",
"file_type": "transferable-public-key",
"key_type": "ed25519",
"valid_until": 1637593318,
"valid_until_iso8601": "2021-11-22T17:02:27+02:00",
"user_ids": [
"Lars Wirzenius",
"<liw@sequoia-pgp.org>",
],
}
$ sq inspect --format=json foo.pgp | jq -r .valid_until
1637593318
$
```

Finally, to determine if `sq` actually does what we claim it does, and
what stakeholders want it to do, Lars is going to add an acceptance
test suite using [Subplot].

  [Subplot]: https://subplot.liw.fi/

## gpg chameleon

If an end user wants to use Sequoia today, they can use `sq`.  But if
they want to use Sequoia as part of their normal workflow, they will
have to wait until the programs they use are ported to Sequoia or
there is some replacement that uses Sequoia.  Even assuming
maintainers would be open to porting their programs to Sequoia,
porting hundreds of programs that use OpenPGP would be a Herculean
effort.  But, more importantly, it would actually contradict one of
our goals: interoperability.  Instead, we have decided to treat GnuPG,
the dominant OpenPGP implementation on the desktop, as a standard API
like POSIX and reimplement `gpg` in terms of Sequoia.  Then, users can
simply replace `gpg` with our chameleon and all programs that use
`gpg` will magically start using Sequoia.

We are not aiming for [bug compatibility].  Justus will implement
standard operations like signing, signature verification, encryption,
decryption, and key management; provide the human-readable interface;
provide the machine-readable interfaces; read configuration files;
parse keyrings and keyboxes; use `gpg-agent`; read trust anchors from
`trustdb.gpg`; and query keyservers and WKDs.  But, we will not
support translations of command-line arguments; we will use our own
[web of trust] [implementation], and not update `trust.db`; and, we
will not support esoteric features like server mode.

  [bug compatibility]: https://en.wikipedia.org/wiki/Bug_compatibility
  [web of trust]: https://sequoia-pgp.gitlab.io/sequoia-wot/
  [implementation]: https://gitlab.com/sequoia-pgp/sequoia-wot

To test interoperability, Justus is implementing a black box,
bi-simulation approach where we treat `gpg` as an oracle and compare a
normalized version of `gpg`'s output with the chameleon's output.
Initially, we had hoped to use existing test suites, but we've since
learned that most tests are too coarse.  They simply check for success
or failure rather than for specific behavior.  This approach is more
robust.

## OpenPGP CA

Heiko presented [OpenPGP CA], which is both a paradigm for how to use
OpenPGP in groups and organizations, and tooling to implement this
paradigm.  OpenPGP CA's main objective is to move away from TOFU and
YOLO authentication and make strong authentication (being sure that
you're using "the right key") easier.

When using PGP or Signal, it's up to the user to figure out if they
have the right key for each correspondent by checking if they have the
right fingerprint or safety number.  This doesn't scale.  Our
proposition is that in many cases it is better for Alice to rely on a
third party to perform most authentication work on her behalf.
Ideally, that third party is aligned with Alice's interests.
Oftentimes in organizations, you already have an IT department that
performs security relevant tasks for users.  If Alice trusts the IT
department to install updates on her machine, for instance, she can
also trust them to perform authentication work on her behalf.  The
basic idea is shown in the following diagram:

<div style="padding: 1em">
  <center>
    <img src="/img/blog/202112-future-of-sequoia-pgp/openpgp-ca.png"
         style="width:80%"
         alt="A diagram showing a user Alice, who is relying on her OpenPGP CA admin to have three other users, authenticated Bob, Carol and Dave.">
    <br>
    <div style="padding: 1em; width: 75%; text-align: left;">
      <b>Figure: If Alice is willing to rely on her organization's
    OpenPGP CA admin, then she doesn't need to manually check the
    fingerprints of her coworkers.  (Stick figures from xkcd (CC BY-NC 2.5).)</b>
    </div>
  </center>
</div>

Thanks to OpenPGP's powerful mechanisms, it is even possible for an
outsider to safely use the organization's CA.  In OpenPGP, it is
possible to only rely on a CA for User IDs from a certain domain.  For
example, if I sometimes want to talk to Eve at the NSA, I can trust
the NSA's CA to certify users *in their organization*, even if I'm a
journalist who suspects that the organization has an adversarial
stance towards me.  In this situation, the NSA CA could only mislead
me about members of their organization, and not mislead me about the
identities of any of my other communication partners.

OpenPGP CA's model is different from the web's PKI where users are
basically forced to use 100s of [untrustworthy] global CAs.  And, it's
different from TOFU, or just hoping for the best without actually
checking anything, i.e. YOLO.  OpenPGP CA strikes a new balance.

  [untrustworthy]: https://sslmate.com/resources/certificate_authority_failures

## Financial Support

Since the start of the project four and a half years ago, the [p≡p
foundation] financially supports the six people who work on Sequoia.
Thanks to the NLnet foundation, we've received grants to work on the
four projects presented here.

  [p≡p foundation]: https://pep.foundation/

We are actively looking for [additional financial support] to
diversify our funding.

You don't need to directly use Sequoia to be positively impacted by
it.  We're focused on creating tools for activists, lawyers, and
journalists who can't rely on centralized authentication solutions.
So, [consider donating].  Of course, if your company is using Sequoia,
consider sponsoring a developer (or two).  Note: if you want to use
Sequoia under a license other than the LGPLv2+, please [contact the
foundation].

  [additional financial support]: https://pep.foundation/support-pEp/
  [OpenPGP CA]: https://openpgp-ca.org/
  [consider donating]: https://pep.foundation/support-pEp/
  [contact the foundation]: https://pep.foundation/contact/
