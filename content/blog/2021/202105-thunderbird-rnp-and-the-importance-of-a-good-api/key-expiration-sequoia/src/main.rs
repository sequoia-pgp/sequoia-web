use std::str::FromStr;

use sequoia_openpgp as openpgp;
use openpgp::cert::prelude::*;
use openpgp::policy::StandardPolicy;

use chrono::prelude::*;

const CERT: &str = "-----BEGIN PGP PRIVATE KEY BLOCK-----\n\
\n\
lFgEYIayyRYJKwYBBAHaRw8BAQdAcfvjx037/k4lGEDeaa0YART02O9EoyY00mXm\n\
YlXcAqIAAP0fjxIS+49uw1g4OKwXNCsYxKG3tvdmGN1R0L+YJ/7vHxBbiNEEHxYK\n\
AIMFgmCGsskFiQWkj70DCwkHCRAHpdhYnySS+UcUAAAAAAAeACBzYWx0QG5vdGF0\n\
aW9ucy5zZXF1b2lhLXBncC5vcmdRgFQyfc9j894Fmn49IUI+mouYhrJ/2OmqUgdW\n\
mLoncAMVCggCmwECHgEWIQST06K432fOS2dJmbgHpdhYnySS+QAAT3cBAIc1+WRk\n\
hu4mxgzl7IYIYQE0ICDUeeHQOiituTiejDf/AQDM8C44g5o3l0yqYN7fmupvKXnI\n\
pA9Wg5nrzLDw/jdtA7QZQWxpY2UgPGFsaWNlQGV4YW1wbGUub3JnPojUBBMWCgCG\n\
BYJghrLJBYkFpI+9AwsJBwkQB6XYWJ8kkvlHFAAAAAAAHgAgc2FsdEBub3RhdGlv\n\
bnMuc2VxdW9pYS1wZ3Aub3Jn9j5II0r/QPCTYe60iEj3OCQBEKOdbMcXi5ME3WPY\n\
K0YDFQoIApkBApsBAh4BFiEEk9OiuN9nzktnSZm4B6XYWJ8kkvkAAAyrAQDM0okI\n\
jixBw2Ef/T/DnoTgoRU9mbz2q0PjicdybbLZGQEAlFEcIp4A0SYasCAAh5djc95v\n\
qtL51AzrwAmzQxeB2A+cWARgiARXFgkrBgEEAdpHDwEBB0CH5SSlu2DJ8GtZRwqH\n\
YEwzu5FLyv7pwCo9UBCsqS4VSAAA/A1xKz7RAic13QXDQFSuBBBuIQp2vjhUSse2\n\
mqY1/x5bDRqI7wQYFggAIBYhBJPTorjfZ85LZ0mZuAel2FifJJL5BQJgiARXAhsC\n\
AIEJEAel2FifJJL5diAEGRYIAB0WIQQXF1DGWQmh1eHWtTIeL1EqD+mVFQUCYIgE\n\
VwAKCRAeL1EqD+mVFSVyAP4hoYqKS47aJaGpnasE29JbBMaEw7Q6iTeaHL8oFpdU\n\
QQEAn/ZsaWtiFxczK/vDaBuxlrIFHI+jdFdOiwujrZYR7QNONQD+L133vRwLRE50\n\
7zLzFw+W/VeexRRTb03zJVWItEKO53MBAICrufVfciL+SkIZ6oHtyqvpy/sG16KM\n\
76OKyctyOfoDnF0EYIayyRIKKwYBBAGXVQEFAQEHQPzhaoKzPYeSXAeQdOf6QilW\n\
pUMzVtlF5/PDnjwHWjwwAwEICQAA/3YFXWls9kPiLSiSitFvetscZfgbBUyrVosj\n\
HqjPPgewDuuIyQQYFgoAewWCYIayyQWJBaSPvQkQB6XYWJ8kkvlHFAAAAAAAHgAg\n\
c2FsdEBub3RhdGlvbnMuc2VxdW9pYS1wZ3Aub3JnSSc/a8ejoK0SvEOfC+FwOD8n\n\
VLuLU+IbHnk1dgzdjr0CmwQCHgEWIQST06K432fOS2dJmbgHpdhYnySS+QAArKkB\n\
AKmMvgYpP61M85bxAMMr/eQRcyTvstXdECkPPuDt30ifAP4volzncrjD3gg2Ik8M\n\
vWR+fBseIzXv/rk2l8HFkpZ6CpxYBGCGtikWCSsGAQQB2kcPAQEHQFy3ioSFXYYW\n\
W/BmEUSH1y01V9/ajm9FpuVWhpdvG9XJAAD/TVO0WHbuOOrnrXXueft3uoSj50pa\n\
LG9ryflSAHSvONgSzIj1BBgWCAAmFiEEk9OiuN9nzktnSZm4B6XYWJ8kkvkFAmCG\n\
tikCGwIFCQABUYAAgQkQB6XYWJ8kkvl2IAQZFggAHRYhBD+E6bsQVwgtrVBLwRQt\n\
VQ5ubfAuBQJghrYpAAoJEBQtVQ5ubfAuj2ABAKk3l69iPY3RFdIEhw8D3IuxCCCL\n\
cfEE3E2LSt25m9D1AQCzCKKhMEhXen1RrXxoPzfitroXNiQ+OAnSrrWbYQAmCSce\n\
AQCezsKIGhgrOOSX2MCohtxeHpP8dHzubA9pRflaft0x9gEAvLVtYGiYAZaSVASA\n\
zzbwFujWYncj6ZhAvKwR1N16iwvHWARghrTQFgkrBgEEAdpHDwEBB0AuPn7/ECVq\n\
ycImmgBRNWtsZJMNZiPTIGlHy9+YNHJM+AABAMgMVXjhc20J7klynuVW+Yx4g2Ay\n\
kXGQyT0rJhjCd4rfD6PCwMgEGBYKAToFgmCGtNAFiQWkj70JEAubLB8FK2xyRxQA\n\
AAAAAB4AIHNhbHRAbm90YXRpb25zLnNlcXVvaWEtcGdwLm9yZ5nIF1j656oIOS/H\n\
H4eysqyJhctCc+dkUU701jOsP50eApsCAh4BvqAEGRYKAG8FgmCGtNAJEHZUwazJ\n\
yi50RxQAAAAAAB4AIHNhbHRAbm90YXRpb25zLnNlcXVvaWEtcGdwLm9yZ5N7tduX\n\
YZVX/XDQkKZHqpTdB2ytvqDGC6K8PkXlKXxfFiEE/d958/i0gscMk0kCdlTBrMnK\n\
LnQAANgBAQDaOc8FLfbaZ4DgctgjO/6uZ+nQBFr6yPENh5S6KbVBiwD/Xf82OxL2\n\
My+SiDnu+V6+dcdyV3VWZfE/GuVquYDc2AQWIQQmp1qEkxLQvn5yi1YLmywfBSts\n\
cgAAVpcA/3TiBWETJpybKqxj+CZXSmbLQPL9tDVEkaqHVuAJuaZoAP9lBYt06pwB\n\
RBZa2BON1DR3BRvwF9sQWLg4ja5fUt3yBMddBGCGtNASCisGAQQBl1UBBQEBB0B+\n\
UGbQp4Tea7UQDw+CPH3K3uz9QPJFRfUNvSRJazT0KgMBCAkAAP92SYORPYwijN4v\n\
jK/wGHMCMHjKRYyUI4oM4iPkamGYeA9i\n\
=Hid3\n\
-----END PGP PRIVATE KEY BLOCK-----\n\
";

fn main() -> openpgp::Result<()> {
    let p = &StandardPolicy::new();

    let cert = Cert::from_str(CERT)?;
    for k in cert.with_policy(p, None)?.keys().subkeys() {
        println!("Key {}: expiry: {}",
                 k.fingerprint(),
                 if let Some(t) = k.key_expiration_time() {
                     DateTime::<Utc>::from(t).to_rfc3339()
                 } else {
                     "never".into()
                 });
    }

    Ok(())
}
