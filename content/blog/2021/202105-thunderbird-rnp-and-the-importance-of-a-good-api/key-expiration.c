// cc -Wall -I../rnp-install/include  -L../rnp-install/lib  key-expiration.c  -lrnp -o key-expiration

#include <rnp/rnp.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

// Subkeys:
// - 1: Subkey with valid binding signature, key doesn't expire.
// - 2: Subkey with valid binding signature, key expires in the future.
// - 3: Subkey with valid binding signature, key is expire.
// - 4: Subkey with cryptographically invalid binding signature, which
//      has a key expiration time subpacket.
// - 5: Subkey with no binding signature.

const char *desc[]
  = { "doesn't expire", "expires", "expired", "invalid sig", "no sig" };

const uint8_t cert[] = "-----BEGIN PGP PRIVATE KEY BLOCK-----\n\
\n\
lFgEYIayyRYJKwYBBAHaRw8BAQdAcfvjx037/k4lGEDeaa0YART02O9EoyY00mXm\n\
YlXcAqIAAP0fjxIS+49uw1g4OKwXNCsYxKG3tvdmGN1R0L+YJ/7vHxBbiNEEHxYK\n\
AIMFgmCGsskFiQWkj70DCwkHCRAHpdhYnySS+UcUAAAAAAAeACBzYWx0QG5vdGF0\n\
aW9ucy5zZXF1b2lhLXBncC5vcmdRgFQyfc9j894Fmn49IUI+mouYhrJ/2OmqUgdW\n\
mLoncAMVCggCmwECHgEWIQST06K432fOS2dJmbgHpdhYnySS+QAAT3cBAIc1+WRk\n\
hu4mxgzl7IYIYQE0ICDUeeHQOiituTiejDf/AQDM8C44g5o3l0yqYN7fmupvKXnI\n\
pA9Wg5nrzLDw/jdtA7QZQWxpY2UgPGFsaWNlQGV4YW1wbGUub3JnPojUBBMWCgCG\n\
BYJghrLJBYkFpI+9AwsJBwkQB6XYWJ8kkvlHFAAAAAAAHgAgc2FsdEBub3RhdGlv\n\
bnMuc2VxdW9pYS1wZ3Aub3Jn9j5II0r/QPCTYe60iEj3OCQBEKOdbMcXi5ME3WPY\n\
K0YDFQoIApkBApsBAh4BFiEEk9OiuN9nzktnSZm4B6XYWJ8kkvkAAAyrAQDM0okI\n\
jixBw2Ef/T/DnoTgoRU9mbz2q0PjicdybbLZGQEAlFEcIp4A0SYasCAAh5djc95v\n\
qtL51AzrwAmzQxeB2A+cWARgiARXFgkrBgEEAdpHDwEBB0CH5SSlu2DJ8GtZRwqH\n\
YEwzu5FLyv7pwCo9UBCsqS4VSAAA/A1xKz7RAic13QXDQFSuBBBuIQp2vjhUSse2\n\
mqY1/x5bDRqI7wQYFggAIBYhBJPTorjfZ85LZ0mZuAel2FifJJL5BQJgiARXAhsC\n\
AIEJEAel2FifJJL5diAEGRYIAB0WIQQXF1DGWQmh1eHWtTIeL1EqD+mVFQUCYIgE\n\
VwAKCRAeL1EqD+mVFSVyAP4hoYqKS47aJaGpnasE29JbBMaEw7Q6iTeaHL8oFpdU\n\
QQEAn/ZsaWtiFxczK/vDaBuxlrIFHI+jdFdOiwujrZYR7QNONQD+L133vRwLRE50\n\
7zLzFw+W/VeexRRTb03zJVWItEKO53MBAICrufVfciL+SkIZ6oHtyqvpy/sG16KM\n\
76OKyctyOfoDnF0EYIayyRIKKwYBBAGXVQEFAQEHQPzhaoKzPYeSXAeQdOf6QilW\n\
pUMzVtlF5/PDnjwHWjwwAwEICQAA/3YFXWls9kPiLSiSitFvetscZfgbBUyrVosj\n\
HqjPPgewDuuIyQQYFgoAewWCYIayyQWJBaSPvQkQB6XYWJ8kkvlHFAAAAAAAHgAg\n\
c2FsdEBub3RhdGlvbnMuc2VxdW9pYS1wZ3Aub3JnSSc/a8ejoK0SvEOfC+FwOD8n\n\
VLuLU+IbHnk1dgzdjr0CmwQCHgEWIQST06K432fOS2dJmbgHpdhYnySS+QAArKkB\n\
AKmMvgYpP61M85bxAMMr/eQRcyTvstXdECkPPuDt30ifAP4volzncrjD3gg2Ik8M\n\
vWR+fBseIzXv/rk2l8HFkpZ6CpxYBGCGtikWCSsGAQQB2kcPAQEHQFy3ioSFXYYW\n\
W/BmEUSH1y01V9/ajm9FpuVWhpdvG9XJAAD/TVO0WHbuOOrnrXXueft3uoSj50pa\n\
LG9ryflSAHSvONgSzIj1BBgWCAAmFiEEk9OiuN9nzktnSZm4B6XYWJ8kkvkFAmCG\n\
tikCGwIFCQABUYAAgQkQB6XYWJ8kkvl2IAQZFggAHRYhBD+E6bsQVwgtrVBLwRQt\n\
VQ5ubfAuBQJghrYpAAoJEBQtVQ5ubfAuj2ABAKk3l69iPY3RFdIEhw8D3IuxCCCL\n\
cfEE3E2LSt25m9D1AQCzCKKhMEhXen1RrXxoPzfitroXNiQ+OAnSrrWbYQAmCSce\n\
AQCezsKIGhgrOOSX2MCohtxeHpP8dHzubA9pRflaft0x9gEAvLVtYGiYAZaSVASA\n\
zzbwFujWYncj6ZhAvKwR1N16iwvHWARghrTQFgkrBgEEAdpHDwEBB0AuPn7/ECVq\n\
ycImmgBRNWtsZJMNZiPTIGlHy9+YNHJM+AABAMgMVXjhc20J7klynuVW+Yx4g2Ay\n\
kXGQyT0rJhjCd4rfD6PCwMgEGBYKAToFgmCGtNAFiQWkj70JEAubLB8FK2xyRxQA\n\
AAAAAB4AIHNhbHRAbm90YXRpb25zLnNlcXVvaWEtcGdwLm9yZ5nIF1j656oIOS/H\n\
H4eysqyJhctCc+dkUU701jOsP50eApsCAh4BvqAEGRYKAG8FgmCGtNAJEHZUwazJ\n\
yi50RxQAAAAAAB4AIHNhbHRAbm90YXRpb25zLnNlcXVvaWEtcGdwLm9yZ5N7tduX\n\
YZVX/XDQkKZHqpTdB2ytvqDGC6K8PkXlKXxfFiEE/d958/i0gscMk0kCdlTBrMnK\n\
LnQAANgBAQDaOc8FLfbaZ4DgctgjO/6uZ+nQBFr6yPENh5S6KbVBiwD/Xf82OxL2\n\
My+SiDnu+V6+dcdyV3VWZfE/GuVquYDc2AQWIQQmp1qEkxLQvn5yi1YLmywfBSts\n\
cgAAVpcA/3TiBWETJpybKqxj+CZXSmbLQPL9tDVEkaqHVuAJuaZoAP9lBYt06pwB\n\
RBZa2BON1DR3BRvwF9sQWLg4ja5fUt3yBMddBGCGtNASCisGAQQBl1UBBQEBB0B+\n\
UGbQp4Tea7UQDw+CPH3K3uz9QPJFRfUNvSRJazT0KgMBCAkAAP92SYORPYwijN4v\n\
jK/wGHMCMHjKRYyUI4oM4iPkamGYeA9i\n\
=Hid3\n\
-----END PGP PRIVATE KEY BLOCK-----\n\
";

const char *fingerprint = "93D3A2B8DF67CE4B674999B807A5D8589F2492F9";

rnp_result_t
test_1(rnp_key_handle_t key) {
  printf("\nTest 1:\n");

  rnp_result_t err;

  size_t sk_count;
  err = rnp_key_get_subkey_count(key, &sk_count);
  if (err) {
    printf("rnp_key_get_subkey_count: %x\n", err);
    return 1;
  }

  // Iterate over the subkeys and print when they expire.
  int i;
  for (i = 0; i < sk_count; i ++) {
    rnp_key_handle_t sk;
    err = rnp_key_get_subkey_at(key, i, &sk);
    if (err) {
      printf("rnp_key_get_subkey_at(%d): %x\n", i, err);
      return 1;
    }

    uint32_t expiration_time;
    err = rnp_key_get_expiration(sk, &expiration_time);
    if (err) {
      printf("#%d (%s). rnp_key_get_expiration: %x\n",
             i + 1, desc[i], err);
    } else {
      printf("#%d (%s) expires %"PRIu32" seconds after key's creation time.\n",
             i + 1, desc[i],
             expiration_time);
    }
  }
  return 0;
}

rnp_result_t
test_2(rnp_key_handle_t key) {
  printf("\nTest 2:\n");

  rnp_result_t err;

  size_t sk_count;
  err = rnp_key_get_subkey_count(key, &sk_count);
  if (err) {
    printf("rnp_key_get_subkey_count: %x\n", err);
    return 1;
  }

  // Iterate over the subkeys and print when they expire.
  int i;
  for (i = 0; i < sk_count; i ++) {
    rnp_key_handle_t sk;
    err = rnp_key_get_subkey_at(key, i, &sk);
    if (err) {
      printf("rnp_key_get_subkey_at(%d): %x\n", i, err);
      return 1;
    }

    bool is_valid = false;
    err = rnp_key_is_valid(sk, &is_valid);
    if (err) {
      printf("rnp_key_is_valid: %x\n", err);
      return 1;
    }

    if (! is_valid) {
      printf("#%d (%s) is invalid, skipping.\n",
             i + 1, desc[i]);
      continue;
    }

    uint32_t expiration_time;
    err = rnp_key_get_expiration(sk, &expiration_time);
    if (err) {
      printf("#%d (%s). rnp_key_get_expiration: %x\n",
             i + 1, desc[i], err);
    } else {
      printf("#%d (%s) expires %"PRIu32" seconds after key's creation time.\n",
             i + 1, desc[i],
             expiration_time);
    }
  }

  return 0;
}

rnp_result_t
test_3(rnp_key_handle_t key) {
  printf("\nTest 3:\n");

  rnp_result_t err;

  size_t sk_count;
  err = rnp_key_get_subkey_count(key, &sk_count);
  if (err) {
    printf("rnp_key_get_subkey_count: %x\n", err);
    return 1;
  }

  // Iterate over the subkeys and print when they expire.
  int i;
  for (i = 0; i < sk_count; i ++) {
    rnp_key_handle_t sk;
    err = rnp_key_get_subkey_at(key, i, &sk);
    if (err) {
      printf("rnp_key_get_subkey_at(%d): %x\n", i, err);
      return 1;
    }

    uint32_t valid_till;
    err = rnp_key_valid_till(sk, &valid_till);
    if (err) {
      printf("rnp_key_valid_till: %x\n", err);
      return 1;
    }

    printf("#%d (%s) valid till %"PRIu32" seconds after epoch; ",
           i + 1, desc[i], valid_till);

    if (valid_till == 0) {
      printf("invalid, skipping.\n");
      continue;
    }

    uint32_t expiration_time;
    err = rnp_key_get_expiration(sk, &expiration_time);
    if (err) {
      printf("rnp_key_get_expiration: %x\n", err);
    } else {
      printf("expires %"PRIu32" seconds after key's creation time.\n",
             expiration_time);
    }
  }

  return 0;
}

int
main(int argc, char *argv[]) {
  rnp_ffi_t ffi;
  rnp_result_t err = rnp_ffi_create(&ffi, "GPG", "GPG");
  if (err) {
    printf("Creating ffi: %x\n", err);
    return 1;
  }

  rnp_input_t input;
  err = rnp_input_from_memory(&input, cert, sizeof(cert) - 1, false);
  if (err) {
    printf("rnp_input_from_memory: %x\n", err);
    return 1;
  }

  err = rnp_import_keys(ffi, input,
                        RNP_LOAD_SAVE_PUBLIC_KEYS|RNP_LOAD_SAVE_SECRET_KEYS,
                        NULL);
  if (err) {
    printf("rnp_import_keys: %x\n", err);
    return 1;
  }

  err = rnp_input_destroy(input);
  if (err) {
    printf("rnp_input_destroy: %x\n", err);
    return 1;
  }

  rnp_key_handle_t key;
  err = rnp_locate_key(ffi, "fingerprint", fingerprint, &key);
  if (err) {
    printf("rnp_locate_key: %x\n", err);
    return 1;
  }

  err = test_1(key);
  if (err) {
    printf("test_1 failed: %x\n", err);
  }

  err = test_2(key);
  if (err) {
    printf("test_2 failed: %x\n", err);
  }

  err = test_3(key);
  if (err) {
    printf("test_3 failed: %x\n", err);
  }

  err = rnp_ffi_destroy(ffi);
  if (err) {
    printf("Destroying ffi: %x\n", err);
    return 1;
  }

  return 0;
}
