---
title: "Talk about OpenPGP interop testing at the IETF 110"
author: Justus
date: 2021-03-16T16:30:00+01:00
banner: img/videos/openpgp-interoperability-test-suite.png
---

I gave a talk at the [IETF 110] about the [OpenPGP Interoperability
Test Suite].  [Slides] and [recording] are available.

  [IETF 110]: https://datatracker.ietf.org/meeting/110/proceedings
  [OpenPGP Interoperability Test Suite]: https://tests.sequoia-pgp.org
  [Slides]: /talks/2021-03-ietf/openpgp-interoperability-test-suite.pdf
  [recording]: https://youtu.be/wfSodl-beQ4?t=1318

The talk introduces the [OpenPGP Interoperability Test Suite],
describes its benefits, how it works, [how to read] the test results,
talks briefly about [results], and [how to join] the effort, improve
the test suite, and [how to run] it.

  [how to read]: https://tests.sequoia-pgp.org/#how-to
  [results]: https://tests.sequoia-pgp.org/#test-summary
  [how to join]: https://gitlab.com/sequoia-pgp/openpgp-interoperability-test-suite
  [how to run]: https://gitlab.com/sequoia-pgp/openpgp-interoperability-test-suite#how-to-run-the-test-suite

Enjoy!
