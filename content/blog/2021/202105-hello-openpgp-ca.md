---
title: "Hello, OpenPGP CA!"
author: Neal
date: 2021-05-12T09:00:00+02:00
banner: img/openpgp-ca-2021-narrow.jpg
---

We are pleased to share that version 0.10.1 of [OpenPGP CA] has been
released!  This is also the first version which is available on
[crates.io].

  [OpenPGP CA]: https://openpgp-ca.org
  [crates.io]: https://crates.io/crates/openpgp-ca

OpenPGP CA is a tool for administrators to create and manage a
decentralized, in-house [certification authority].  OpenPGP CA makes
it possible and easy for an organization to delegate authentication
decisions to someone they can rely on, like their system
administrator.  And, external groups and individuals can use these CAs
to authenticate certificates for just that organization using
OpenPGP's [scoped trust signatures].  OpenPGP CA also helps with key
discovery and key updates by managing a [Web Key Directory][] (WKD).

  [certification authority]: https://en.wikipedia.org/wiki/Certificate_authority
  [scoped trust signatures]: https://datatracker.ietf.org/doc/html/rfc4880#section-5.2.3.14
  [Web Key Directory]: https://datatracker.ietf.org/doc/html/draft-koch-openpgp-webkey-service-11

<!--more-->

## What is OpenPGP CA?

Let's say your organization wants to protect email that is sent
internally as well as email that is exchanged with important partners
who also use the same encryption standard.  To make the scenario
concrete, let's say that there are 50 people in your organization, and
that you have a small IT team.  How do you do it?

To achieve this using OpenPGP, the IT department could organize
something like a [CryptoParty].  First, someone would hold an hour or
two lecture explaining the basics behind [confidentiality, integrity,
and authentication], why they are important, and how they are achieved
in OpenPGP.  Then, there would be an install party where everyone
installs the right mail client or add-on.  And, finally, everyone
would create a key, [compare each others' fingerprints], and send and
open a few mails.

  [CryptoParty]: https://en.wikipedia.org/wiki/CryptoParty
  [compare each others' fingerprints]: https://en.wikipedia.org/wiki/Key_signing_party
  [confidentiality, integrity, and authentication]: https://en.wikipedia.org/wiki/Information_security

This approach might be effective for a few tech enthusiasts.  But my
experience is that it won't even reach most of the people who perceive
a clear and urgent need for this type of protection.  And, as for the
rest of the employees, they'll be bored, but perhaps they'll be happy
to have had a few hours off to daydream.

An alternative that give users similar benefits, but with much less
hassle, is a CA-based scheme.  Using a CA shifts the burden of
authenticating keys from individuals to a trusted third party.  If
everyone were to verify everyone else's fingerprint in our 50 person
organization, they'd each verify 49 fingerprints.  That's 1250 (`= 50 *
49`) verifications in all!  If instead everyone relied on a CA, then
an administrator would have to check the 50 fingerprints and install
the CA certificate on 50 computers.  That's a lot less work for
everyone involved.

Of course, in this scenario it is still necessary to have everyone
create a key.  But, this is a one-time procedure, which can be done as
part of an on-boarding process.  And, it is necessary for the
administrator to maintain a directory (e.g., WKD or LDAP) and ensure
that the users' software is configured to use it.  But none of those
things require ongoing efforts from the end users.

If you're a privacy activist, you're possibly thinking: NO!  I don't
want a CA!  I've heard of how [Symantec] has repeatedly misissued
certificates, and governments like [Turkey] have been caught forging
certificates.  The interests of these enormous CAs are not aligned
with the interests of their users; they can't be relied on!

  [Symantec]: https://wiki.mozilla.org/CA:Symantec_Issues
  [Turkey]: https://blog.mozilla.org/security/2013/01/03/revoking-trust-in-two-turktrust-certficates/

And, that, folks, is exactly the problem that OpenPGP CA solves.
Instead of using a third-party CA, OpenPGP CA makes it easy to run an
in-house CA.  Since a system administrator already typically has
administrator rights on all of their users' computers, the users only
need to place marginally more trust in the system administrator.  And
the win is enormous: not only are users able to send encrypted emails
to their co-workers, but the keys have actually been [authenticated],
which is an *essential* property of secure communication that is being
increasingly overlooked in the age of Signal.  And, OpenPGP CA
achieves this using OpenPGP's *existing* mechanisms.  You can read
more about OpenPGP CA in its extensive [documentation].

  [authenticated]: https://en.wikipedia.org/wiki/Authentication
  [documentation]: https://openpgp-ca.org/background/

Also, because OpenPGP CA makes it easy for organizations to sign each
other's CA keys and scope those certifications to just a single
organization, it is easy for an administrator to ensure that their
users are able to fully automatically authenticate keys for users at
other organizations.  Individuals can, of course, do the same.

## A Few Details

OpenPGP CA works as follows.  OpenPGP CA helps an administrator sign
each user's certificate, and publishes them in a WKD.  The
administrator can even sign certificates of other OpenPGP CA instances
(thereby bridging groups), and third-party certificates.  User's that
then set the CA to be a trusted introducer (in GnuPG terminology: they
set the key's ownertrust to "FULLY", or tsign the CA's key) will be
able to authenticate many of their regular communication partners
without any effort.  This means that communication is not only much
more likely to be encrypted, but the keys are actually authenticated.

<div style="padding: 1em">
  <center>
    <img src="/img/blog/202105-hello-openpgp-ca/bridge.svg"
         style="width:90%"
         alt="Two OpenPGP CA-using organizations and various certifications, which OpenPGP CA helps create. alice@alpha.org tsigns alpha.org's CA certificate.  alpha.org's CA creates a trust signature for beta.org's CA certificate, which is scoped to beta.org.  beta.org's CA certifies a certificate for bob@beta.org. Alice can use these certifications to certify the certificate for bob@beta.org.">
    <br>
    <div style="padding: 1em; width: 75%">
      <b>Figure: Two OpenPGP-CA using organizations and various
      certifications that OpenPGP CA helps create.  Using these
      certifications, Alice is able to authenticate a key for Bob.</b>
    </div>
  </center>
</div>

The above figure shows two organizations, `alpha.org` and `beta.org`,
who use OpenPGP CA, and various certifications, which OpenPGP CA helps
create.  In the figure, Alice (`alice@alpha.org`) has tsigned
`alpha.org`'s CA certificate.  `alpha.org`'s CA tsigned `beta.org`'s
CA certificate and scoped the certification to `beta.org`.
`beta.org`'s CA certified a certificate for `bob@beta.org`.  Alice can
use these certifications to certify the certificate for Bob
(`bob@beta.org`).  Note: if `beta.org`'s CA has certified a
certificate for `carol@gamma.org`, Alice would not use that
certification to certify a certificate for `carol@gamma.org`, because
the trust signature that her CA made is scoped to `beta.org`.  This is
how it is possible to use an organization's CA without fully trusting
them.

OpenPGP CA is a tool for administrators.  Since it manages sensitive
data, it should be run in a well-protected environment.  To further
simplify deployment, OpenPGP CA also ships a [REST daemon], which can
easily be integrated into an existing intranet backend, or a web
portal.  In this case, a member of the organization authenticates
their certificate to the administrator by proving they can login to
the web portal and upload a certificate.  OpenPGP CA then only
certifies a User ID if it matches the user's email address.  This
feature can also be used by a club to easily certify and distribute
their members' OpenPGP certificates.

  [REST daemon]: https://openpgp-ca.org/doc/restd/
