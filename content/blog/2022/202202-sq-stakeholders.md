---
title: "Looking for sq stakeholders"
author: Lars
date: 2022-02-01T12:00:00+02:00
banner: /img/blog/programmable-sq.jpg
keywords:
- sq
---

Do you use `sq` or want to use it in the future? Please volunteer to
help guide its development.

Sequoia isn't just a library. It just takes a library-first approach.
Sequoia's command-line interface, which exposes a lot of the library's
functionality, is called `sq`. It already exists in a basic form, but a
lot of functionality is missing. You can help with that.

<!--more-->

A crucial factor in developing good software is knowing what it needs
to do, and how. This means talking to people who use it, or would like
to use it. That is, talking to people who have an interest in the
software doing what they need it to do, doing it well, and being
comfortable for them to use.

If that's you, then please help by joining a "stakeholder group" for
`sq`. Stakeholder here means someone with an interest in the future of
the software. You qualify as a stakeholder if you already use `sq` or
want to use it in the future.

I would like to gather a group of volunteers to guide `sq` in a good
direction. Please volunteer if you'd be willing to put some effort
into this. I will interview the stakeholders about how they use `sq`
and OpenPGP, and do user testing with them by observing them using
development versions of `sq` to achieve specific goals. Both
interviews and user testing will occur over the Internet using video
conferencing. I expect this will take an hour or two at a time, two to
four times over the next few months. In return, the developers of `sq`
will be better informed to make decisions about its development.

To volunteer as a stakeholder, please send me email by mid-February
saying so (`liw@sequoia-pgp.org`). I'm going to keep the list of
stakeholders private, but I will make public summaries of results of
interviews, user testing, etc.

(To be clear: stakeholders have an advisory role. They don't get to
make decisions. Sequoia developers will make decisions regarding their
work, but stakeholders will influence those decisions by providing
feedback on changes.)
