---
title: "JSON support for sq keyring list, sq wkd url"
author: Lars
date: 2022-07-21T16:00:00+03:00
banner: /img/blog/programmable-sq.jpg
keywords:
- sq
---

The Sequoia command line tool `sq` has gained support for the `sq
keyring list` and `sq wkd url` commands.

<!--more-->

~~~sh
$ sq --output-format=json keyring list liw.pgp
{
  "sq_output_version": {
    "major": 0,
    "minor": 0,
    "patch": 0
  },
  "keys": [
    {
      "fingerprint": "01329E6448F73322374600FE914E0DE96A7328A8",
      "primary_userid": "Lars",
      "userids": [
        "<liw@liw.fi>"
      ]
    }
  ]
}

$ sq --output-format=json wkd url me@example.com
{
  "sq_output_version": {
    "major": 0,
    "minor": 0,
    "patch": 0
  },
  "advanced_url": "https://openpgpkey.example.com/.well-known/openpgpkey/example.com/hu/s8y7oh5xrdpu9psba3i5ntk64ohouhga?l=me",
  "direct_url": "https://example.com/.well-known/openpgpkey/hu/s8y7oh5xrdpu9psba3i5ntk64ohouhga?l=me"
}
~~~

`sq` allows the format of the output to be chosen with the
`--output-format` option, or the `SQ_OUTPUT_FORMAT` environment
variable. The default is "human readable".

The machine-readable output formats are versioned. The version can be
chosen with the `--output-version` option or `SQ_OUTPUT_VERSION`
environment variable. The default is the latest version. The output
version is independent of the `sq` version.

We will be adding JSON support to other commands in the future. If you
would like JSON support in specific commands, let us know. We've
chosen JSON as the most common format, but if you'd like us to support
another machine-readable format, do tell us.


## Note

This work is supported by a grant from the NLnet foundation from the
NGI Assure fund, financially supported by the European Council.
