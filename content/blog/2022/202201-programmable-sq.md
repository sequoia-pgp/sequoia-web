---
title: "New project starting: Programmable sq"
author: Lars
date: 2022-01-25T16:00:00+02:00
banner: /img/blog/programmable-sq.jpg
keywords:
- sq
---


[NLnet Foundation]: https://nlnet.nl/
[NGI Assure]: https://nlnet.nl/assure/
[project plan]: https://liw.fi/sequoia/sq-nlnet/
[#808]: https://gitlab.com/sequoia-pgp/sequoia/-/issues/804

The [NLnet Foundation] has granted me funding (from the [NGI Assure]
fund, financially supported by the European Council) to improve the
Sequoia `sq` program in three ways.

I will add important missing functionality, especially compared to
GnuPG. This work will be guided by feedback from actual and potential
users and the wisdom of Sequoia developers.

I will also add a JSON API to allow sq to be used from scripts.
Ideally, other programs would use the Sequoia library directly,
however, using `sq` from other programs should be easy and secure, and
JSON is a better format than parsing textual output or ad hoc
structured data formats

I will additionally document the acceptance criteria of `sq` and how
they are verified automatically, to make sure `sq` does the right
thing for its users, and to help keep `sq` working far into the
future.

I have now started the work, and am about to reach the first
milestone.

<!--more-->

The first milestone was defined as:

> _When this milestone has been reached:_
> An initial list of acceptance criteria for sq have been documented
> based on functionality at the beginning of the project. This will
> include how to automatically verify that sq fulfills the criteria. The
> verification is done by Sequoia CI for every change.
>
> This milestone produces a foundational building block for all other
> development: it sets up the way how sq functionality and acceptance
> criteria will be documented and verified in an automated way. This is
> best done early in the process so that it supports development work,
> rather than done at the end, when it's more work to do.
>
> Implementation steps:
>
> - write acceptance criteria and verification scenarios for as much of
>   existing sq functionality as there is time for
>
> Time estimate: 5 days
>
> Deliverable: blog post linking to acceptance criteria documentation

The work was split into several merge requests:
[!1208](https://gitlab.com/sequoia-pgp/sequoia/-/merge_requests/1208),
[!1210](https://gitlab.com/sequoia-pgp/sequoia/-/merge_requests/1210),
[!1213](https://gitlab.com/sequoia-pgp/sequoia/-/merge_requests/1213),
[!1216](https://gitlab.com/sequoia-pgp/sequoia/-/merge_requests/1216),
[!1217](https://gitlab.com/sequoia-pgp/sequoia/-/merge_requests/1217),
[!1218](https://gitlab.com/sequoia-pgp/sequoia/-/merge_requests/1218),
and
[!1219](https://gitlab.com/sequoia-pgp/sequoia/-/merge_requests/1219).


Some aspects of `sq` remain unverified by the test suite. I opened
[#808] to track them so they're not forgotten.
Test coverage can be measured in various ways. I opt to measure how
many features of `sq` are verified by the acceptance test suite,
rather than how much of the code is run by the test suite. I feel this
works better when testing the program as a whole.

My detailed [project plan] is on my personal site, but I will blog
here about each milestone I reach.

The next milestone is about building a group of people who have an
interest in using `sq` in any way and want to help make it good. I
will be publishing a separate blog post about this, soon, but if you'd
like to volunteer early, please email me at `liw@liw.fi`.
