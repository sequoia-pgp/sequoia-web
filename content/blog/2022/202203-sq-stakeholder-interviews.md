---
title: "sq stakeholders interviews: summary"
author: Lars
date: 2022-03-02T11:00:00+02:00
banner: /img/blog/programmable-sq.jpg
keywords:
- sq
---

[looking for volunteers to be interviewed]: https://sequoia-pgp.org/blog/2022/02/01/202202-sq-stakeholders/
[pass]: https://www.passwordstore.org/
[gopass]: https://www.passwordstore.org/#other
[PIV]: https://en.wikipedia.org/wiki/FIPS_201

Last month I was [looking for volunteers to be interviewed][] as
stakeholders for sq. The interviews happened last week and this is an
anonymized summary of what I was told. I promised to make the summary
anonymous to let the volunteers speak more freely.

<!--more-->


I didn't have a fixed list of questions. Instead, we had somewhat
free-form discussions about using cryptography in general, OpenPGP in
particular, practices around exchanging and authenticating
certificates (public keys), and related topics.

* I interviewed ten volunteers about sq and Sequoia. There would've been
  a couple more, but there were communication issues, so those
  interviews didn't happen.

* This is a small group, and is self-selected to feel positive towards the
  Sequoia PGP project. No meaningful statistical analysis of the
  results can be made. Conclusions should not be taken very far.

* They live across Europe and in the US. Alas, there weren't people
  from other parts of the world. It's not a greatly diverse group; if
  this exercise is repeated, it would be good to try for a more diverse
  set of people to get a more diverse set of experiences.

* The stakeholders all have technical backgrounds involving computing.
  Several are software developers by profession, a couple are more
  oriented towards sysadmin/devops, a couple are managers, and one is a
  librarian. A couple of people were additionally politically active on
  the grass-root level.

* Many mentioned using a Yubikey for storing their private subkeys. They
  didn't like setting it up, but are happy it works.

* Many use or have used OpenPGP with email, for signing, encryption,
  or both. The most common email software mentioned was Thunderbird
  with Enigmail. However, use of OpenPGP with email is fading away.
  Signal is the most common replacement.

  - the [Schleuder](https://schleuder.org/) re-encrypting mailing list
    manager was mentioned by several

  - email is still generally considered important, and  worth supporting
    and improving

* Most said they use an OpenPGP subkey on a hardware token for SSH.

* Several were concerned about securing software supply chains.

  - however, most of the software developers don't sign git commits,
    and rely on the security of the (internal) git server instead

  - there seems to be general interest in supply chain solutions

* Only a couple of people mentioned using key servers. Several said
  the servers had become useless for them. Few had heard about
  [keys.openpgp.org](https://keys.openpgp.org/). People mostly
  exchanged and authenticated other people's certificates on a 1:1
  basis, or only within their organization, or by relying on
  trust-on-first-use.

  - a couple went much further to authenticate, because of their
    particular needs for security

  - a couple of people said they avoid key servers to avoid making
    their social graph public

* Most had never heard of [OpenPGP CA](https://openpgp-ca.org/), but
  almost everyone liked the idea, and was interested in learning more,
  or trying it out for their organization. One person said they expect
  it to become a game changer.

* Most had at least a vague idea of what
  [WKD](https://wiki.gnupg.org/WKD) (Web Key Directory) is, several
  used it to retrieve keys, but few were using it to publish keys. I
  got the impression publishing a WKD is maybe difficult to arrange
  for many organizations due to internal policy reasons.

* Most mentioned doing at least something to verify that downloaded
  software was OK.

  - usually they rely on package managers

  - sometimes they only check checksums, without digital signatures

  - some look for a detached signature and corresponding public key,
    and most of them trust the key on first use

* Many had written, or were using, shell script or other wrappers
  around gpg to automate common operations. Nobody liked it,
  especially parsing the output of gpg.

* Usability in the OpenPGP ecosystem was raised by pretty much everyone.

  - this is not just about GnuPG specifically being hard to use, but
    much software in the ecosystem suffering from this to some degree

  - some of the cause is from concepts, such as Web of Trust
    traditionally being under-specified and often misunderstood

  - also, there seemed to be a feeling that the ecosystem was lacking
    in good, well-known, maintained documentation for current versions
    of tools, but also including documentation not specific to tools

  - lack of relevant examples was mentioned by several people

* Those who had heard of
  [SOP](https://datatracker.ietf.org/doc/html/draft-dkg-openpgp-stateless-cli-01)
  (see also [git](https://gitlab.com/dkg/openpgp-stateless-cli)), the
  Stateless OpenPGP command line interface, really liked it.

* Many use [pass][] or its variants, like [gopass][]. Those who do would like
  them to use Sequoia. [git-secret](https://github.com/sobolevn/git-secret) was
  also mentioned by a couple of people.

* Things they liked about GnuPG:

  - it's free software and implements an important standard

  - it's possible to audit

  - it provides incredible value

  - it's widely available and widely used

  - it's available for most platforms

  - once it's set up, it works reliably

  - it has out-of-the-box support for hardware tokens

  - it has SSH integration

* Things they didn't like about GnuPG:

  - it's complicated to learn and set up

  - it's easy to make mistakes when using it

  - it's difficult to use from other programs

  - many if its aspects are hard to understand

  - it lacks support for [PIV][]

  - it does not encrypt for all valid subkeys of the recipient, by default

  - using it in headless environments cause several people problems

  - it's so dominant it stifles development of the openpgp ecosystem

* Things they said about the Sequoia project or the software it
  produces:

  - the [interoperability test suite](https://tests.sequoia-pgp.org/) is a good thing

  - the recent [Web of Trust](https://gitlab.com/sequoia-pgp/sequoia-wot) work is important

  - use of Rust is praised, even when it makes the software cumbersome
    to install or integrate with

  - one person would like Sequoia to provide a stable C API to the
    library; others either didn't mention it or plan to integrate
    using sq

  - the library-first approach of Sequoia is appreciated

  - several people would like Sequoia to provide drop-in replacements
    for existing tools, or at least main uses of those, to allow easy,
    gradual migration

  - the project is needed

  - the project gives them hope

* Things they'd like the Sequoia project to provide:

  - seamless support for hardware tokens

  - support for using multiple hardware tokens for subkeys of the same
    main key, possibly the same subkey on different tokens

  - ease of use from other programs

* Things they said would help them try sq or migrate to sq:

  - packages for all main operating systems, preferably from those
    systems' usual package repositories, preferably also backports for
    older releases

    - Debian, Fedora COPR, Void Linux, and FreeBSD mentioned

# Conclusion

My conclusion is that the Sequoia approach of building tools that are
easy to use well is worth it, but also that there's a need to educate
and market the tools we have already built.

This doesn't change the direction of my own project of making better
to use from other programs, except I shall try to blog and otherwise
make more noise about what I achieve.

Building a better mousetrap is not enough, if nobody knows about it.

# Note

This work was supported by a grant from the [NLnet
foundation](https://nlnet.nl/) from the [NGI Assure
fund](https://nlnet.nl/assure/), financially supported by the European
Council.
