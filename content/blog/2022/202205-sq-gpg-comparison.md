---
title: "sq feature comparison with gpg"
author: Lars
date: 2022-05-11T12:00:00+02:00
banner: /img/blog/programmable-sq.jpg
keywords:
- sq
---

The [GnuPG][] command line tool [`gpg`][] is the most popular
implementation of the [OpenPGP][] specification. The [Sequoia PGP][]
project produces the corresponding [`sq`][] tool, and that tool is
very much in its early stages. In the long run, we want `sq` to become
so capable it has a comparable feature set to `gpg`. This blog post is
a comparison of what the two tools can do.

[GnuPG]: https://gnupg.org/
[`gpg`]: https://www.gnupg.org/documentation/manpage.html
[OpenPGP]: https://datatracker.ietf.org/doc/html/rfc4880
[Sequoia PGP]: https://sequoia-pgp.org/
[`sq`]: https://docs.sequoia-pgp.org/sq/index.html

<!--more-->

This comparison is not a detailed feature comparison table, comparing
option to option and command to command. Such a comparison would be
interesting, but not necessarily meaningful: the two programs have
such different approaches that a comparison table is hard to produce, and
would be hard to make sense of. Thus, the approach taken here is to
describe both programs at a higher level.

The goal of this exercise is to get an actionable list of changes to
`sq` to make it better suited as an everyday tool for cryptography.
The comparison is meant to be a short-cut to extensive user studies.

The comparison is divided into four parts: a short description of
`gpg`, and of `sq`, a discussion of the differences, and a list of
concrete proposals for changing `sq`.

For this blog post, we assume the reader has at least a basic
understanding of OpenPGP.

## gpg in summary

`gpg` is close to being a complete solution for using OpenPGP. It
allows a user to do the basic cryptographic operations
(encrypt/decrypt, sign/certify/verify, etc), as well as generate keys,
manage collections of keys or certificates, manage sharing of
certificates. `gpg` can handle keys stored on the local file system,
or on hardware smartcards. It can exchange certificates with key
servers.

`gpg` manages collections of keys and certificates in "key rings",
which it stores, by default, in a hidden directory in the user's home
directory. By default, it only uses keys and certificates stored
there. The hidden directory also has configuration files.

Some of `gpg` functionality is actually implemented as daemons, which
`gpg` starts automatically, if they aren't already running. For
example, all cryptographic operations using private keys is done in
`gpg-agent`.

`gpg` tries hard to require a user to specify only a minimal amount of
information for common commands. It gets configured defaults and other
information from its implicit hidden directory. Thus, the minimal
command to encrypt a file for a given recipient is:

~~~sh
gpg -e -r Bob msg.txt
~~~

Here, `gpg` knows to look for a certificate with a User ID
containing "Bob", and use that for the encryption. The user only needs
to specify the minimal number of things, none of which can be
inferred: the operation (encrypt), the recipient, and the file.

`gpg` has some support for being used in a programmatic way: the
`--with-colons` and `--status-fd` options.

The `gpg` command line interface is based on hundreds of options. Some
of them are by now only interesting for historical reasons.

In general, `gpg` tries to handle every possible use case for any
possible user.

## sq in summary

 `sq` allows, and currently requires, the user to be more verbose than
`gpg` does. To encrypt a file for a given recipient:

~~~sh
sq encrypt --recipient-cert=bob.pgp --output=msg.pgp msg.txt
~~~

`sq` does not have an implicit key ring where to look for the
recipient's certificate. The file with the key or certificate needs to
be named by the user. `sq` also does not have a configuration file.
`sq` has support for exchanging certificates with key servers.

There is an upcoming programmatic interface for `sq`: [my project to
add JSON support](https://liw.fi/sequoia/sq-nlnet/) to `sq` is about
doing exactly that.

The `sq` command line interface is based on subcommands, some of which
have options.


## Discussion of differences between gpg and sq

* Configuration file: The approach taken by `sq` so far doesn't
  preclude adding a configuration file, if and when one makes
  sense. However, configuration files may make programmatic use more
  difficult, as the configuration may affect what a program does.

* Implicit key rings, and certificates: the Sequoia project is working
  on separate stores of public certificates and private keys. These
  are meant to be usable by other OpenPGP implementations as well.

* Configurable cryptography: `gpg` allows the user to be quite
  specific as to key types and sizes, algorithms, etc. `sq` provides a
  few choices that its developers deem necessary. The Sequoia
  developers prefer this approach, because it tends to be difficult to
  learn enough to make informed choices here.

* Hardware smartcards: `gpg` has pretty good support for this. Sequoia
  is working on that support, including for using the TPM chip in most
  PCs.

## Proposals for changing sq (and Sequoia in general)

Note that this list only covers the high level changes. Details are
expected to be discussed at length when the change actually gets
implemented. (Issue numbers provided as a guide: they may not be
exhaustive.)

* Allow a user to add User IDs to an existing key. There is no need to
  remove User IDs, because certificates are an append-only data
  structure. Revoking a User ID is already possible.
  ([#483](https://gitlab.com/sequoia-pgp/sequoia/-/issues/483))

* Allow a user to set, unset, or change the expiration time of a key
  or subkey.
  ([#848](https://gitlab.com/sequoia-pgp/sequoia/-/issues/848))

* Improve adding certifications.
  ([#637](https://gitlab.com/sequoia-pgp/sequoia/-/issues/637))

* Allow a user to add subkeys. (They can already be revoked.)
  ([#849](https://gitlab.com/sequoia-pgp/sequoia/-/issues/849))

* Allow a user to extract a certificate in the SSH public key format.
  ([#850](https://gitlab.com/sequoia-pgp/sequoia/-/issues/850))

* Allow a user to set a signature or a certification to expire.
  ([#103](https://gitlab.com/sequoia-pgp/sequoia/-/issues/103), kind of)

* Allow a user to clean up / minimize certificates for sharing.
  ([#810](https://gitlab.com/sequoia-pgp/sequoia/-/issues/810))

* Integrate support for public and private key stores into `sq`.
  (No issue, but work is happening. This will probably include a lot
  of design work as it touched most aspects of `sq`.)

* Integrate support for using smartcards and TPM.
  (No issue, but work is happening.)

* Allow users to add/remove/revoke/export User Attributes, especially
  social handles, but maybe also photos. Also, extract them to a file
  or similar.
  ([#853](https://gitlab.com/sequoia-pgp/sequoia/-/issues/853))

* Allow a user to clean/minimize export certificates.
  ([#758](https://gitlab.com/sequoia-pgp/sequoia/-/issues/758))

* Allow a user to change capability flags for a key.
  ([#851](https://gitlab.com/sequoia-pgp/sequoia/-/issues/851))

* Allow a user to retrieve keys using DANE, LDAP, and NTDS.

* Discuss whether group functionality is needed in `sq`.

* Discuss whether `sq` should support something like pinentry.

* Allow a user to debug `sq` by using logging.
  ([#851](https://gitlab.com/sequoia-pgp/sequoia/-/issues/852))

* Allow a user to find keys from various places online.
  ([#651](https://gitlab.com/sequoia-pgp/sequoia/-/issues/651))
