---
title: "Just Say No To War"
date: 2022-02-28T14:05:41+01:00
author: Justus
banner: /blog/2022/02/28/202202-no-to-war/kyiv.jpg
---

The Sequoia PGP project condemns the war that the Russian government
is waging against our friends in Ukraine.

<!--more-->

<img style="float: right; padding: 1em;" src="ukraine-needs-you.jpg" />

We are deeply worried that decades of demilitarization efforts
are being reverted in a few short days, with e.g. Germany announcing
plans to increase their military budget.

Further, we note that centralized messaging services like Facebook and
Twitter were both censoring Russian news sources, and in turn being
censored in Russia.  Our reading is that centralized services are way
too fragile to freely distribute information.

In 2018, in the wake of the annexation of Crimea and in the early days
of Sequoia, we visited Kyiv attending the [Decentralization
Unchained](https://deltaxi.noblogs.org/) unconference that brought
together human rights activists, digital security trainers,
journalists, artists, and software developers.

These were happier times for the beautiful city of Kyiv that we
encourage everyone to visit if you get the chance.  But now,
Ukraine needs our support, especially if you can reach people in
Russia who may not have even heard of the war.  Do not be silent.
Speak up.
