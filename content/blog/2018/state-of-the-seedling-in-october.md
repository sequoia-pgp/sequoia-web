---
title: "State of the Seedling"
author: Justus
date: 2018-10-12T17:03:06+02:00
banner: img/sequoia-banner.jpeg
---
I'd like to inform you about the recent progress on your favorite
OpenPGP implementation.  It has been five weeks since I last wrote an
update, and there are exciting developments to report.

<!--more-->

Sequoia now collects direct key signatures, and honors subpackets
affecting the primary key (like key flags) usually found on user id
binding signatures.  Although not specified by RFC4880, this is
clearly a better place for such subpackets, because they have nothing
to do with user ids.  There is at least one other implementation that
does this, Riboses fork of NetPGP.

The public key store now supports lookup by keyid, and the frontend
exploits that for signature verification.

We did a great deal of refactoring, e.g. moving code around, refining
types, using our own abstractions more instead of accessing fields
directly.

Sequoia now tracks signature levels.  A level 0 signature is directly
over data, a level 1 signature notarizes level 0 signatures and data,
etc.  The frontend, sq, displays this information.  Furthermore, the
frontend now can add signatures to existing messages, and notarize
existing signatures.

Sequoia gained a new buffered reader implementation for files.  The
buffered reader trait allows us to compose readers into a stack of
filters.  At the bottom of each stack is a reader actually reading
data from somewhere.  The implementation for files can be specialized
to use platform specific methods to read the file, e.g. on POSIX
systems it tries to mmap the file.

The TPK module now supports, honors, and creates revocation
signatures.

The streaming verifier has been improved, and it gained a friend, the
streaming decryptor (that also verifies any signatures).  Both objects
use a trait that the user needs to implement to fetch TPKs and secrets,
and implement a verification policy.

The 'sq dump' command now draws a tree using line-drawing characters
to illustrate the tree structure of OpenPGP messages.

Sequoia now tries to protect secrets like secret key MPIs, session
keys, and passwords by introducing specialized types that currently
overwrite their memory when they go out of scope.  In the future, we
may also mlock these regions.  As a nice side-effect, these types
provide clear semantics of function arguments.

When encrypting using 'sq encrypt', we optionally sign the data.  In
this case, we also emit the proposed intended recipient subpacket.
The streaming decryptor now honors these subpackets.

The lower-level parser API has been simplified.  We now track the
packet's path in the parser, and no longer return the depths when
transitioning to the next packet.  This greatly improves the ergonomic
of the parser.

Our FFI-boundary received some attention driven by Neal's effort to
port the pEp engine to Sequoia.  The port is quite complete at this
point, merely the actual encryption and decryption operations are
missing.  This is notable because the pEp engine relies on the OpenPGP
implementation for storing keys.

Finally, Sequoia now supports the AEAD bits of RFC4880bis, that is we
handle the preferred AEAD algorithm subpacket, the AEAD feature flag,
SKESKv5 packets, and AEAD Encrypted Data Packets.  We can encrypt and
decrypt using the unencumbered EAX mode.  The streaming encryptor uses
the AED packet in favor of the SEIP+MDC packets if all recipients
announce support for AEAD in their TPKs.

That's it for now, thanks for flying with Sequoia :)
Justus
