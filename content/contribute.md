+++
title = "Contribute"
description = "How to contribute"
keywords = ["contribute", "getting involved", "hacking"]
+++

## Getting Started

Most Sequoia projects are licensed under the [LGPL
2.0](https://www.gnu.org/licenses/old-licenses/lgpl-2.0.html) or
later.  [As with all free
software](https://www.gnu.org/philosophy/free-sw.en.html), you are
free to run, copy, distribute, study, change and improve Sequoia.

We also welcome contributions, such as reporting issues, bug fixes,
new features, and improvements to our documentation.  Before getting
started on a project, you should [contact us](/contact).

If you plan to contribute a larger feature on behalf of a commercial
entity, we'll probably only agree to integrate it and maintain it, if
you commit to a long-term maintenance contract with us.  Even if you
don't plan to contribute a feature, but do integrate Sequoia, you can
significantly lower your liability by obtaining a maintenance
contract.  Please send a mail to
[sales@sequoia-pgp.org](mailto:sales@sequoia-pgp.org) for details.

## Projects

Most of our projects are [hosted by
Gitlab](https://gitlab.com/sequoia-pgp).  This includes
[`sequoia-openpgp`](https://gitlab.com/sequoia-pgp/sequoia), our
low-level library; [`sq`](https://gitlab.com/sequoia-pgp/sequoia), our
primary command-line interface; [the
Chameleon](https://gitlab.com/sequoia-pgp/sequoia-chameleon-gnupg),
our drop-in replacement for `gpg`; and, [the
Octopus](https://gitlab.com/sequoia-pgp/sequoia-octopus-librnp), our
OpenPGP backend for Thunderbird.  A few of our projects are hosted by
[GitHub](https://github.com/sequoia-pgp).  For a high-level overview,
please see our [projects page](/projects).

## Contributions

Contributions to Sequoia are governed by the Developer Certificate of
Origin, which can be obtained from https://developercertificate.org/.
A copy is reproduced below, for your convenience.

### Developer Certificate of Origin

```text
Developer Certificate of Origin
Version 1.1

Copyright (C) 2004, 2006 The Linux Foundation and its contributors.

Everyone is permitted to copy and distribute verbatim copies of this
license document, but changing it is not allowed.


Developer's Certificate of Origin 1.1

By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I
    have the right to submit it under the open source license
    indicated in the file; or

(b) The contribution is based upon previous work that, to the best
    of my knowledge, is covered under an appropriate open source
    license and I have the right under that license to submit that
    work with modifications, whether created in whole or in part
    by me, under the same open source license (unless I am
    permitted to submit under a different license), as indicated
    in the file; or

(c) The contribution was provided directly to me by some other
    person who certified (a), (b) or (c) and I have not modified
    it.

(d) I understand and agree that this project and the contribution
    are public and that a record of the contribution (including all
    personal information I submit with it, including my sign-off) is
    maintained indefinitely and may be redistributed consistent with
    this project or the open source license(s) involved.
```
