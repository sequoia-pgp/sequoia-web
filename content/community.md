+++
title = "The Sequoia Community"
keywords = ["about us", "who are we", "community"]
+++

A number of people working in the OpenPGP space are involved in the
Sequoia project.  Some work directly on Sequoia, some work on projects
using Sequoia, some are otherwise working on improving the state of
the OpenPGP ecosystem.

{{< community >}}

# Artist

All caricatures are by <a href="https://cartoonclub.de/about/">Manuel Ruz</a>
from <a href="https://cartoonclub.de/">Cartoon Club</a>.
