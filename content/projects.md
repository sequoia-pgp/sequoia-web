+++
title = "Projects"
keywords = ["code", "git", "related projects", "community"]
+++

{{< heading 2 "Projects under Sequoia's Umbrella" >}}

{{< project
	name="sequoia-openpgp"
	homepage=""
	repo="https://gitlab.com/sequoia-pgp/sequoia"
	crate="sequoia-openpgp"
	docs="https://docs.sequoia-pgp.org/sequoia_openpgp/"
	bugs="https://gitlab.com/sequoia-pgp/sequoia/issues"
	status="/status/"
	guide=""
	contribute=""
	donate=""
	summary="A low-level OpenPGP library."
	description="This crate aims to provide a complete implementation of OpenPGP as defined by RFC 9580 as well as some extensions."
>}}

{{< project
	name="sq"
	homepage=""
	repo="https://gitlab.com/sequoia-pgp/sequoia-sq"
	crate="sequoia-sq"
	docs="https://sequoia-pgp.gitlab.io/user-documentation"
	bugs="https://gitlab.com/sequoia-pgp/sequoia-sq/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="Our primary command-line interface for Sequoia."
	description="<i>sq</i> provides encryption, decryption, signature creation and verification, key and certificate management, and key server and <i>WKD</i> interactions with a convenient git-style subcommand interface."
>}}

{{< project
	name="sqv"
	homepage=""
	repo="https://gitlab.com/sequoia-pgp/sequoia-sqv"
	crate="sequoia-sqv"
	docs="https://docs.sequoia-pgp.org/sqv/"
	bugs="https://gitlab.com/sequoia-pgp/sequoia-sqv/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="A command-line tool for OpenPGP signature verification."
	description="<i>sqv</i> verifies detached OpenPGP signatures.  It is a replacement for <i>gpgv</i>.  Unlike <i>gpgv</i>, it can take additional constraints on the signature into account.  It is designed for sotware distribution systems like Debian.  See <a href=\"https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=872271\">this bug report</a>."
>}}

{{< project
	name="sqop"
	homepage=""
	repo="https://gitlab.com/sequoia-pgp/sequoia-sop"
	crate="sequoia-sop"
	docs="https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/"
	bugs="https://gitlab.com/sequoia-pgp/sequoia-sop/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="An implementation of SOP using Sequoia."
	description="<i>sqop</i> implements the <a href=\"https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/\">Stateless OpenPGP Command Line Interface</a>, which specifies encryption, decryption, signature creation and verification, and basic key and certificate management services."
>}}

{{< project
	name="Chameleon"
	homepage=""
	repo="https://gitlab.com/sequoia-pgp/sequoia-chameleon-gnupg"
	crate="sequoia-chameleon-gnupg"
	docs="https://gitlab.com/sequoia-pgp/sequoia-chameleon-gnupg#sequoias-reimplementation-of-the-gnupg-interface"
	bugs="https://gitlab.com/sequoia-pgp/sequoia-chameleon-gnupg/issues"
	status="https://gitlab.com/sequoia-pgp/sequoia-chameleon-gnupg#status"
	guide=""
	contribute=""
	donate=""
	summary="A reimplementation of gpg using Sequoia."
	description="The Chameleon is a re-implementation and drop-in replacement of <i>gpg</i> and <i>gpgv</i> using the Sequoia OpenPGP implementation."
>}}

{{< project
	name="Octopus"
	homepage=""
	repo="https://gitlab.com/sequoia-pgp/sequoia-octopus-librnp"
	crate="sequoia-octopus-librnp"
	docs="https://gitlab.com/sequoia-pgp/sequoia-octopus-librnp#a-sequoia-based-openpgp-backend-for-thunderbird"
	bugs="https://gitlab.com/sequoia-pgp/sequoia-octopus-librnp/-/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="A Sequoia-based OpenPGP Backend for Thunderbird."
	description="The Octopus is a drop-in replacement for the RNP library supported and shipped by Thunderbird.  In addition to implementing the OpenPGP primitives that Thunderbird uses, it also reads the user's GnuPG keyring, transparently uses gpg-agent, and uses the web of trust."
>}}


{{< project
	name="OpenPGP interoperability test suite"
	homepage="https://tests.sequoia-pgp.org"
	repo="https://gitlab.com/sequoia-pgp/openpgp-interoperability-test-suite"
	crate=""
	docs=""
	bugs="https://gitlab.com/sequoia-pgp/openpgp-interoperability-test-suite/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="A test suite designed to chart and improve interoperability of OpenPGP implementations."
	description="The test suite uses a simple black-box API based on the <a href=\"https://tools.ietf.org/html/draft-dkg-openpgp-stateless-cli-01\">Stateless OpenPGP Command Line Interface</a> to test a number of OpenPGP implementations."
>}}

{{< project
	name="dump.sequoia-pgp.org"
	homepage="https://dump.sequoia-pgp.org"
	repo="https://gitlab.com/sequoia-pgp/dump.sequoia-pgp.org"
	crate=""
	docs=""
	bugs="https://gitlab.com/sequoia-pgp/dump.sequoia-pgp.org/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="An OpenPGP packet dumper as a service."
	description=""
>}}

{{< heading 2 "Downstream Users" >}}

The follow projects use Sequoia, but are not developed by us.  (If you
want to include a project in this list, please [get in
contact](https://gitlab.com/sequoia-pgp/sequoia-web/issues/).)


{{< project
	name="Hagrid"
	homepage="https://keys.openpgp.org"
	repo="https://gitlab.com/keys.openpgp.org/hagrid"
	crate=""
	docs="https://keys.openpgp.org/about/usage"
	bugs="https://gitlab.com/keys.openpgp.org/hagrid/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="A verifying OpenPGP key server."
	description="Hagrid is an OpenPGP key server, which is used for the distribution and discovery of OpenPGP certificates.  In contrast to conventional key severs, Hagrid does not publish identity information without the consent of the user, and allows the removal of identity information.  Hagrid is used by <a href=\"https://keys.openpgp.org\">keys.openpgp.org</a>."
>}}

{{< project
	name="RPM Package Manager"
	homepage="http://rpm.org/"
	repo="https://github.com/rpm-software-management/rpm"
	crate=""
	docs="http://rpm.org/documentation.html"
	bugs="https://github.com/rpm-software-management/rpm/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="A package management system."
	description="The RPM Package Manager (RPM) is a powerful package management system.  It is capable of building computer software from source into easily distributable packages; installing, updating and uninstalling packaged software; querying detailed information about the packaged software, whether installed or not; and verifying integrity of packaged software and resulting software installation."
>}}

{{< project
	name="sett"
	repo="https://gitlab.com/biomedit/sett-rs"
	crate="sett"
	docs="https://biomedit.gitlab.io/docs/sett/"
	bugs="https://gitlab.com/biomedit/sett-rs/issues"
	status=""
	guide="https://www.biomedit.ch/dam/jcr:90cab4c4-0727-43f3-ab22-e55b43da52e4/2021-Fact-sheet-sett.pdf"
	contribute=""
	donate=""
	summary="An application that facilitates and automates data packaging, encryption, and transfer."
    description="sett stands for “Secure Encryption and Transfer Tool.”  It is developed as part of Switzerland's <a href=\"https://www.biomedit.ch\">BioMedIT project</a>, and is primarily intended for researchers and hospitals in Switzerland to securely transfer data within the <a href=\"https://sphn.ch\">Swiss Personalized Health Network</a> (SPHN), but it can be used as a general secure transfer tool.  sett is available both as a desktop and a command line application."
>}}

{{< project
	name="Secure Drop"
	homepage="https://securedrop.org/"
	repo="https://github.com/freedomofpress/securedrop"
	crate=""
	docs="https://docs.securedrop.org/en/stable/"
	bugs="https://github.com/freedomofpress/securedrop/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="A tool for sharing and accepting documents securely."
	description="SecureDrop is an open-source whistleblower submission system that media organizations can use to securely accept documents from and communicate with anonymous sources."
>}}
