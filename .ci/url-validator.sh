#!/bin/bash
#
# SPDX-FileCopyrightText: 2023 David Runge <dave@sleepmap.de>
# SPDX-License-Identifier: GPL-3.0-or-later

set -euo pipefail

if (( $# < 1)); then
    printf "Provide at least one file or directory as argument!\n" >&2
    exit 1
fi

paths=()
for path in "$@"; do
    paths+=("$(realpath "$path")")
done

readonly blocklist=(
    example.com
    twitter.com
    https://contribution.pep.foundation/contribute/
    https://gitea.pep.foundation/
    "http://webchat.freenode.net?randomnick=1&channels=%23sequoia&prompt=1"
    https://dump.sequoia-pgp.org
)

url_is_blocked() {
    local url="$1"
    local blocked=1
    for block in "${blocklist[@]}"; do
        if [[ "$url" == *$block* ]]; then
            blocked=0
            printf "Skip %s\n" "$url" >&2
        fi
    done
    return $blocked
}

filter_blocked() {
    while read url
    do
        if ! url_is_blocked "$url"
        then
            echo "$url"
        fi
    done
}

check_url() {
    local url="$1"
    local curl_options=(
        --max-time 10
        --retry 3
        --fail
        --header 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/109.0'
        --header 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8'
        --header 'Accept-Language: en-US,en;q=0.5'
        --header 'Accept-Encoding: gzip, deflate, br'
        --header 'DNT: 1'
        --header 'Connection: keep-alive'
        --header 'Upgrade-Insecure-Requests: 1'
        --header 'Sec-Fetch-Dest: document'
        --header 'Sec-Fetch-Mode: navigate'
        --header 'Sec-Fetch-Site: none'
        --header 'Sec-Fetch-User: ?1'
        --insecure
        --location
        --show-error
        --silent
    )

    printf "Check %s\n" "$url"
    if ! curl "${curl_options[@]}" --head "$url" > /dev/null; then
        printf "Can not retrieve headers from %s, try to get entire page.\n" "$url" >&2
        curl "${curl_options[@]}" "$url" > /dev/null
    fi
}

export -f check_url

rg -oIN -tmd -P "https?://[][[:alnum:]._~:/?@\!&'*+,;%=\-]+" "${paths[@]}" \
    | sort \
    | uniq -u \
    | filter_blocked \
    | parallel check_url
