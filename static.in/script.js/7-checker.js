document.addEventListener('DOMContentLoaded', function() {
    const checker = document.getElementById('checker');
    if (checker) {
        checker.addEventListener('submit', async function(e) {
            e.preventDefault();
            const email = checker.querySelector('input[type="email"]').value;
            const output = checker.querySelector('ul');
            output.textContent = 'Checking...';
            try {
                const resp = await fetch("https://metacode.biz/openpgp/web-key-directory/checker", {
                    method: "POST",
                    body: JSON.stringify({ email })
                });
                const json = await resp.json();
                output.innerHTML = '';
                json.lint.map(lint => {
                    const li = document.createElement('li');
                    li.className = lint.level;
                    li.textContent = lint.message;
                    return li;
                }).forEach(output.appendChild.bind(output));
            } catch (e) {
                output.textContent = 'Error while checking the key: ' + e;
            }
        });
    }
});
